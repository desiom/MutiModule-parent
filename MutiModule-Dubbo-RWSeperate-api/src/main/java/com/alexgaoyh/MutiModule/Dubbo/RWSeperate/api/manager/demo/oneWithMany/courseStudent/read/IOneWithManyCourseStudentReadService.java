package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.demo.oneWithMany.courseStudent.read;

import java.util.List;
import java.util.Map;

import com.MutiModule.common.myPage.MyPageView;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.courseStudent.OneWithManyCourseStudentKey;

/**
 * OneWithManyCourseStudentKey 模块 读接口
 * @author alexgaoyh
 *
 */
public interface IOneWithManyCourseStudentReadService {

	int selectCountByMap(Map<Object, Object> map);

	List<OneWithManyCourseStudentKey> selectListByMap(Map<Object, Object> map);

    /**
     * 获取分页实体信息部分
     * @param map	参数传递，封装部分过滤参数
     * @return
     */
    MyPageView<OneWithManyCourseStudentKey> generateMyPageViewVO(Map<Object, Object> map);
    
    /**
     * 删除
     * @param map
     * @return
     */
    int deleteByMap(Map<Object, Object> map);
    
}
