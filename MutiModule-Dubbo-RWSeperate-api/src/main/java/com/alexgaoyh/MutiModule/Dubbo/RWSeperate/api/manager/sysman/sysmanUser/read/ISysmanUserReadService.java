package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanUser.read;

import java.util.List;
import java.util.Map;

import com.MutiModule.common.myPage.MyPageView;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanUser.SysmanUser;

/**
 * SysmanUser 模块 读接口
 * @author alexgaoyh
 *
 */
public interface ISysmanUserReadService {

	int selectCountByMap(Map<Object, Object> map);

    List<SysmanUser> selectListByMap(Map<Object, Object> map);

    SysmanUser selectByPrimaryKey(String id);
    
    /**
     * 获取分页实体信息部分
     * @param map	分页操作类
     * @return
     */
    MyPageView<SysmanUser> generateMyPageViewVO(Map<Object, Object> map);
    
}
