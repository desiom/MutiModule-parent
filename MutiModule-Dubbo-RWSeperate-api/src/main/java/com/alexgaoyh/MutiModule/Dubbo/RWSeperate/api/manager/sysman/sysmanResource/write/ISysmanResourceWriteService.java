package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanResource.write;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanResource.SysmanResource;

/**
 * SysmanResource 模块 写接口
 * 
 * @author alexgaoyh
 *
 */
public interface ISysmanResourceWriteService {

	int deleteByPrimaryKey(String id);

	int insert(SysmanResource record);

	int insertSelective(SysmanResource record);

	int updateByPrimaryKeySelective(SysmanResource record);

	int updateByPrimaryKey(SysmanResource record);

}
