package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.dict.dictionary.write;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.DictDictionary;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.view.DictDictionaryWithItemView;

/**
 * DictDictionary 模块 写接口
 * @author alexgaoyh
 *
 */
public interface IDictDictionaryWriteService {

	int deleteByPrimaryKey(String id);

	int insert(DictDictionary record);

	int insertSelective(DictDictionary record);

	int updateByPrimaryKeySelective(DictDictionary record);

	int updateByPrimaryKey(DictDictionary record);
	
	/**
	 * 插入 字典表 ，一对多关联关系
	 * @param dictDictionaryView
	 * @return
	 */
	int insertDictDictionaryView(DictDictionaryWithItemView dictDictionaryView);
	
	/**
	 * 更新  字典表 ，一对多关联关系
	 * @param dictDictionaryView
	 * @return
	 */
	int updateDictDictionaryView(DictDictionaryWithItemView dictDictionaryView);
}
