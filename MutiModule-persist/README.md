MutiModule-persist部分：
	1、首先创建对应对应的数据库表结构，之后修改 genetatorConfig.xml 部分，targetPackage对应的包路径，tableName对应的数据库表名称，项目（MutiModule-persist）右键，
	run As 选择 mybatis-generator:generate，执行完毕后，生成对应的 模板文件；
	
	
	**********************20150711 以下的分页支持功能，已经废弃，改为通过 mybatis-generator-maven-plugin 的扩展插件实现 PaginationPlugin
	2、在模板文件中添加对应的分页支持：
		2.1:  *Example.java 文件增加  protected MyRowBounds myRowBounds(get set方法省略)；
			序列化相关操作 implements Serializable
		
		2.2:  *Mapper.xml 文件增加如下部分， 
				<sql id="myRowBoundsSQL">
					<if test="myRowBounds != null">
						limit ${myRowBounds.offset}, ${myRowBounds.limit}
					</if>
				</sql>
				
			并将这个  myRowBoundsSQL 添加到 selectByExample包含的select语句中
				<!-- alexgaoyh begin -->
				<include refid="myRowBoundsSQL" />
			    <!-- alexgaoyh end -->
						
		2.3:
			/**  20150608 removed 接下来的bean定义交由MapperScannerConfigurer代替，较少代码段书写
				使用MapperScannerConfigurer代替一个个的bean定义
			module-persist-bean.xml 里面增加对应的bean 设定
				<bean id="*Mapper" class="org.mybatis.spring.mapper.MapperFactoryBean">  
			        <property name="sqlSessionFactory" ref="sqlSessionFactory" />  
			        <property name="mapperInterface" value="com.alexgaoyh.MutiModule.persist.*.*Mapper" />  
				</bean>
			*/
			
			
		2.3： 进行junti测试 *MyBatisTest.java
				//定义查询过滤类
				DemoExample demoExample = new DemoExample();
				demoExample.setOrderByClause("id desc");
				
				//定义分页相关，第一页，一页10条
				MyRowBounds myRowBounds0 = new MyRowBounds(1,10);
				demoExample.setMyRowBounds(myRowBounds0);
				//查询总记录数，list集合
				int count0 = demoMapper.countByExample(demoExample);
				List<Demo> demoList0 = demoMapper.selectByExample(demoExample);
				
	**********************20150711 以上的分页支持功能，已经废弃，改为通过 mybatis-generator-maven-plugin 的扩展插件实现 PaginationPlugin
	
	
	3： 树形结构
		3.1 *。mapper.xml里面，数据库查询如下所示：
			<!-- 树形结构查询  alexgaoyh begin -->
			<resultMap type="com.MutiModule.common.vo.TreeNode" id="treeNodeResultMap">  
		        <id property="id" column="id"/>  
		        <result property="text" column="text"/>  
		        <result property="isLeaf" column="isLeaf"/>  
		        <result property="iconCls" column="iconCls"/>  
		        <result property="state" column="state"/>  
		        <!-- 查询父模块 -->  
		        <association property="parent" column="parentId" select="getTreeNodeById" />  
		        <!-- 查询子模块 -->  
		        <collection property="children" column="id" select="getChildrenTreeNode" />  
		    </resultMap>  
		      
		    <select id="selectTreeNodeBySysmanResourceId" parameterType="int" resultMap="treeNodeResultMap">  
		        select * from sysmanresource where id = #{id};
		    </select>  
		      
		    <select id="getTreeNodeById" parameterType="int" resultMap="treeNodeResultMap">  
		        select * from sysmanresource where id = #{id};
		    </select>  
		      
		    <select id="getChildrenTreeNode" parameterType="int" resultMap="treeNodeResultMap">  
		        select * from sysmanresource where parentId = #{id};
		    </select>
		    <!-- 树形结构查询  alexgaoyh end -->			
		3.2 *Mapper.java文件里面，相关查询如下所示：
				//id为需要根据哪个节点开始，对此节点下的数据进行树形结构转换，测试时可以递归操作，对树形结构进行深度优先遍历。
				//方法名称对应xml文件里面的select语句部分
				List<TreeNode> selectTreeNodeBySysmanResourceId(Integer id);
			
			
	**********************20150711 以下的逻辑删除功能，已经废弃，改为通过 mybatis-generator-maven-plugin 的扩展插件实现	DeleteLogicByIdsPlugin
	
	4: 删除操作，本例的删除只是逻辑删除(deleteFlag)状态改变
		4.1	*。Mapper.java 类增加
				public int deleteLogicByIds(@Param("deleteflag")Integer deleteFlag, @Param("ids")Integer[] ids);
		4.2 *。Mpaaer.xml 增加
				<update id="deleteLogicByIds">
					update sysmanuser
					set deleteFlag = #{deleteflag,jdbcType=INTEGER}
					where id in
					<foreach item="item" index="index" collection="ids" open="(" separator="," close=")">
				            #{item}
				        </foreach>
				  </update>
		4.3 增加测试方法，返回更新状态的数据
		
	**********************20150711 以上的逻辑删除功能，已经废弃，改为通过 mybatis-generator-maven-plugin 的扩展插件实现	DeleteLogicByIdsPlugin	
		
	5： sysmanRole类，重写父类方法 hashCode() equals()
		保证在对形如集合处理操作的时候，保证id相同的话，两个对象实际是相同的，例如 List.removeALL(?) 
		
	6： 使用MapperScannerConfigurer 代替一个个的bean定义。
	
#20150711
	扩展  mybatis-generator-maven-plugin 功能，在自动生成代码时添加分页功能：
		com.MutiModule.common.mybatis.plugin.PaginationPlugin mysql 分页扩展插件
			com.MutiModule.common.vo.mybatis.pagination.Page 分页插件依赖的分页类 
			
		com.MutiModule.common.mybatis.plugin.DeleteLogicByIdsPlugin 自定义的扩展插件
			实现增加一个方法，方法名称为 deleteLogicByIds， 在 接口文件和XML的sql文件中，都增加自定义的方法。
			
#20170713
	使用  mybatis-generator-maven-plugin 扩展插件，对RBAC相关资源进行重新代码生成
	认真阅读 generatorConfig.xml 文件的注释部分，有助于有效地自动生成代码结构
		注意， 关联关系表结构设定为符合主键，这样，关联关系表结构生成时，会自动生成 *Key.java 的类文件；
			单表结构的部分，注意id, deleteFlag, createTime 部分为必须字段。
			
	mybatis-generator-maven-plugin	用来指定自动生成主键的属性（identity字段或者sequences序列）。如果指定这个元素，MBG在生成insert的SQL映射文件中插入一个<selectKey>元素
		<generatedKey column="id" sqlStatement="Mysql" identity="true" type="post"/>
							
#20150725
	增加 BaseLocation 	省市区 数据库相关
	
#20150726
	修复BaseLocation 相关bug部分（Long转为 Integer）,新增方法处理
	persist层，generatorConfig.xml 增加两个参数设置，	beginningDelimiter endingDelimiter 将默认生成的sql.xml文件中默认的  (") 去除；
	
#20150728
	修复 beginningDelimiter endingDelimiter 为（"） 时候的部分bug，将这一部分旧代码存在的bug进行修复；
	
#20150801
	修复 *Mapper.java 文件中，存在的(")部分，修复遗留的代码bug;	如下：
		addCriterion("\"id\" >=", value, "\"id\"");		 改为 		addCriterion("id >=", value, "id");
		
	增加 DemoTransientListStringTest 单元测试部分，	可以用来保存多个图片路径的json串
		此单元测试主要测试  @Transient 注解，可以保存至数据库字段中的值为json字符串(泛型为List<String>)，之后再取出json数据的时候，将这一部分的json串转换为原始数据格式进行输出；
	增加	DemoTransientEnumTest  单元测试部分，可以用来保存多个enum类型的数据格式，json串
		此单元测试主要测试  @Transient 注解，可以保存至数据库字段中的值为json字符串(泛型为 List<Enum>)，之后再取出json数据的时候，将这一部分的json串转换为原始数据格式进行输出；
		
#20150803
	generatorConfig.xml 中，增加Enum typeHandler部分，增加删除标识的相关操作，不再使用常量表示相关状态；
		
		<columnOverride column="DELETEFLAG" property="deleteFlag" 
        		javaType="com.MutiModule.common.vo.enums.DeleteFlagEnum" 
        		typeHandler="com.alexgaoyh.MutiModule.persist.typeHandler.DeleteFlagEnumHandler"></columnOverride>
	并且编写单元测试，测试通过  
	
#20150815
	增加一对多关联关系查询：
		generatorConfig.xml 代码生成器，<table>标签增加 alias 属性，避免多表间关联查询出现 未明确定义列的错误  
		增加 班级-学生 一对多关联查询相关的单元测试部分，并且支持 example 进行条件过滤处理；
		
#20150921
	修改数据库连接池 改为 druid
		从 org.apache.commons.dbcp.BasicDataSource 替换为 com.alibaba.druid.pool.DruidDataSource
	并进行相关参数配置 详见 https://github.com/alibaba/druid
	
#20150922
	druid persist层   配置防御SQL注入攻击
	druid persist层   数据库密码加密
		即便对数据库密码进行加密操作，在 com.alibaba.druid.filter.FilterChainImpl.connection_connect(Properties info) 方法中，还是能够查询到解密后的password
		使用  	com.alibaba.druid.filter.config.ConfigTools	com.alibaba.druid.filter.config.ConfigFilter 对数据库密码进行加密相关操作；
		
#20151109
	从pl/sql查询字段类型为number并显示为科学计数法的计数方法的问题
		在pl/sql developer中->tools->preferences->sql windows->number fields tochar,选中该选项即可。 
		
	在MutiModule-common模块中，新增 com.MutiModule.common.mybatis.plugin.mutiDatasource.MutiDatasourcePaginationPlugin 类文件
		这个类文件用来生成不仅仅支持mysql,同样支持oracle的分页操作方法；	并且通过测试
			注意点在: mybatis databaseIdProvider 的使用，针对不同的数据库方言，调用不同的数据库方法；
				生成的  *Mapper.xml 的sql文件中： 包含：  <if test="page != null and _databaseId == 'oracle'">  这种形式的段落，可据此判断不同的数据库连接；
			
		至此，在通过 mybatis-generator-maven-plugin 插件生成的文件中，即支持不同数据情况下的数据库分页操作，同理，其他数据库类型的操作，也可根据这种方式进行书写；
		
	测试的话，修改 pom.xml 文件中的 profile（oracle mysql）的activeByDefault 默认激活状态进行测试，单元测试为 ： com.alexgaoyh.MutiModule.persist.mutiDatabase.MutiDatabaseTest
		前提： 在不同的数据库中（oracle mysql），打开  MutiDatabase.sql 生成相对应的表结构，之后可以进行测试；	多种数据库环境下，通用分页功能已完成；