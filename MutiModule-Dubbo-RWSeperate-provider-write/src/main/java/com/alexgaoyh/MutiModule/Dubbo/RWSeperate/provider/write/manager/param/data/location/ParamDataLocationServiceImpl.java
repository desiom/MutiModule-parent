package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.write.manager.param.data.location;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.param.data.location.write.IParamDataLocationWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.param.data.location.ParamDataLocation;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.param.data.location.ParamDataLocationMapper;

@Service(value = "paramDataLocationService")
public class ParamDataLocationServiceImpl implements IParamDataLocationWriteService{
	
	@Resource(name = "paramDataLocationMapper")
	private ParamDataLocationMapper mapper;

	@Override
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(ParamDataLocation record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(ParamDataLocation record) {
		return mapper.insertSelective(record);
	}

	@Override
	public int updateByPrimaryKeySelective(ParamDataLocation record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(ParamDataLocation record) {
		return mapper.updateByPrimaryKey(record);
	}
	
}
