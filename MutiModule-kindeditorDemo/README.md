#20150610
	实现与  MutiModule-ueditorDemo 相似的功能；
	添加新的富文本编辑器的功能  kindeditor  https://github.com/kindsoft/kindeditor 
	
#20150704
	删除 MutiModule-kindeditorClass 模块，将相关的类文件移动到 MutiModule-common 模块中
	讲原先MutiModule-upload 模块删除，其中文件上传部分js jsp文件移动到 MutiModule-kindeditorDemo 内部
	其中jsUpload.jsp 中对应的文件上传URL 采用 kindeditor 部分，形成统一的文件上传方法。
	
#20150706
	此模块增加图片处理相关，ImgServlet.java 文件
	形如通过 kindeditor 进行图片上传之后，返回的图片链接为 ：
	http://127.0.0.1:8080/MutiModule-kindeditorDemo/attached/image/20150706/20150706122314_613.jpg
	此时可以在文件后缀后面增加形如 !100_100 的方式，表示对原图片进行 宽100 长100 的处理
	
	如果浏览器进行 http://127.0.0.1:8080/MutiModule-kindeditorDemo/attached/image/20150706/20150706122314_613!200_200.jpg
	的访问，那么，将会对 20150706122314_613.jpg 原图进行 宽:高 =200:200 的处理，并且返回到浏览器
	
#20150707
	移除kindeditorDemo模块内所有资源文件，确保此模块仅仅为DEMO测试部分，不包含任何资源文件
	将此模块下的js文件移动至 MutiModule-kindeditor 模块内部
	
	其余模块如果想使用kindeditor香港功能，需要修改模块的pom.xml web.xml 文件，具体文件内容如MutiModule-kindeditorDemo 的模块。
	即可引入kindeditor香港操作
	
#20150717
	整理此DEMO模块的一些细节功能处理方法，此DEMO测试模块主要地方在于 pom.xml web.xml ,其他webapp模块如果想实现类似功能，需要注意这两个文件的部分代码逻辑
	
	修改上传文件的两个页面 index.jsp jsUpload.jsp 两个文件，
		在调用上传servlet 的时候，增加两个入参，?contextPath=demo&detailPath=anonymous
			contextPath： 项目上下文路径，用来表明是哪个项目模块的上传文件
			detailPath: 用户级别的路径，用来区分不同用户上传的资料文件，如果是未登录用户，使用 anonymous 作为入参，这样，能够区分不同用户的上传功能；
			
#20150723
	增加 jquery.uploadify.*.js 的文件处理功能，测试完毕
