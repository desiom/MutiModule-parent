<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <title>ECharts</title>
</head>

<body>
    <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
    <div id="main" style="height:400px"></div>
    <!-- ECharts单文件引入 -->
    <script src="http://echarts.baidu.com/build/dist/echarts.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery/jquery.js"></script>
	<script src="${pageContext.request.contextPath}/js/sockjs-0.3.4.js"></script>
    <script src="${pageContext.request.contextPath}/js/stomp.js"></script>
    <script type="text/javascript">
		var context_ = '<%=request.getContextPath()%>';
		var optionJSON = ${optionJSON};
    
        // 路径配置
        require.config({
            paths: {
                echarts: 'http://echarts.baidu.com/build/dist'
            }
        });
        
		// 使用
        require(
            [
                'echarts',
                'echarts/chart/bar' // 使用柱状图就加载bar模块，按需加载
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main'));
                myChart.setOption(optionJSON); 

                function connect() {
                    var socket = new SockJS(context_ + '/initJson');
        			stompClient = Stomp.over(socket);
                    stompClient.connect({}, function(frame) {
                        console.log('Connected: ' + frame);
                        stompClient.subscribe('/topic/showResult', function(calResult){
                        	showResult(JSON.parse(calResult.body));
                        });
                    });
                };
                
                connect();
                
                function showResult(optionJSON) {
                	myChart.setOption(eval('('+optionJSON+')')); 
                }
                
            }
        );
    </script>
    
</body>