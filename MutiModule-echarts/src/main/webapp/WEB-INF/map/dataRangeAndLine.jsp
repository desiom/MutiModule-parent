<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
	<!-- 引入 ECharts 文件 -->
    <script src="${pageContext.request.contextPath}/js/echarts/echarts.js" charset="UTF-8"></script>
    <script src="${pageContext.request.contextPath}/js/jquery/jquery.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/base.css" />
<style type="text/css">
html, body {
	height: 100%;
}

.content {
	width: 100%;
	height: 100%;
}

.cont_main {
	width: 100%;
	height: 100%;
	background: #0066CC;
}

.cont_left {
	display: inline-block;
	width: 70%;
	height: 100%;
	background: #FFFFFF;
	float: left;
}

.cont_top {
	height: 70%;
	width: 100%;
}

.cont_bot {
	height: 30%;
	width: 100%;
	overflow: scroll;
	background: #996699;
}

.cont_right {
	display: inline-block;
	width: 30%;
	height: 100%;
	overflow: scroll;
	background: #006600;
	float: left;
}
</style>
</head>
<body>
	<div class="content ">
		<div class="cont_main clearfix ">
			<div class="cont_left">
				<div class="cont_top" id="myMap"></div>
				<div class="cont_bot" id="myLog"></div>
			</div>

			<div class="cont_right">
				<font style="font-size: 100px;">订单量</font>
				<br>
				<input type="text" id="myAllConut" value="1"/>
				<br>
				<font style="font-size: 50px;">地图列表中订单数更新</font>
				<br>
				<div id="myOrderLog"></div>
			</div>
		</div>

	</div>
	<script type="text/javascript">
	
		var myChart;
		
		var geoCoordVar = {
			'上海': [121.4648,31.2891],
			'东莞': [113.8953,22.901],
			'东营': [118.7073,37.5513],
			'中山': [113.4229,22.478],
			'临汾': [111.4783,36.1615],
			'临沂': [118.3118,35.2936],
			'丹东': [124.541,40.4242],
			'丽水': [119.5642,28.1854],
			'乌鲁木齐': [87.9236,43.5883],
			'佛山': [112.8955,23.1097],
			'保定': [115.0488,39.0948],
			'兰州': [103.5901,36.3043],
			'包头': [110.3467,41.4899],
			'北京': [116.4551,40.2539],
			'北海': [109.314,21.6211],
			'南京': [118.8062,31.9208],
			'南宁': [108.479,23.1152],
			'南昌': [116.0046,28.6633],
			'南通': [121.1023,32.1625],
			'厦门': [118.1689,24.6478],
			'台州': [121.1353,28.6688],
			'合肥': [117.29,32.0581],
			'呼和浩特': [111.4124,40.4901],
			'咸阳': [108.4131,34.8706],
			'哈尔滨': [127.9688,45.368],
			'唐山': [118.4766,39.6826],
			'嘉兴': [120.9155,30.6354],
			'大同': [113.7854,39.8035],
			'大连': [122.2229,39.4409],
			'天津': [117.4219,39.4189],
			'太原': [112.3352,37.9413],
			'威海': [121.9482,37.1393],
			'宁波': [121.5967,29.6466],
			'宝鸡': [107.1826,34.3433],
			'宿迁': [118.5535,33.7775],
			'常州': [119.4543,31.5582],
			'广州': [113.5107,23.2196],
			'廊坊': [116.521,39.0509],
			'延安': [109.1052,36.4252],
			'张家口': [115.1477,40.8527],
			'徐州': [117.5208,34.3268],
			'德州': [116.6858,37.2107],
			'惠州': [114.6204,23.1647],
			'成都': [103.9526,30.7617],
			'扬州': [119.4653,32.8162],
			'承德': [117.5757,41.4075],
			'拉萨': [91.1865,30.1465],
			'无锡': [120.3442,31.5527],
			'日照': [119.2786,35.5023],
			'昆明': [102.9199,25.4663],
			'杭州': [119.5313,29.8773],
			'枣庄': [117.323,34.8926],
			'柳州': [109.3799,24.9774],
			'株洲': [113.5327,27.0319],
			'武汉': [114.3896,30.6628],
			'汕头': [117.1692,23.3405],
			'江门': [112.6318,22.1484],
			'沈阳': [123.1238,42.1216],
			'沧州': [116.8286,38.2104],
			'河源': [114.917,23.9722],
			'泉州': [118.3228,25.1147],
			'泰安': [117.0264,36.0516],
			'泰州': [120.0586,32.5525],
			'济南': [117.1582,36.8701],
			'济宁': [116.8286,35.3375],
			'海口': [110.3893,19.8516],
			'淄博': [118.0371,36.6064],
			'淮安': [118.927,33.4039],
			'深圳': [114.5435,22.5439],
			'清远': [112.9175,24.3292],
			'温州': [120.498,27.8119],
			'渭南': [109.7864,35.0299],
			'湖州': [119.8608,30.7782],
			'湘潭': [112.5439,27.7075],
			'滨州': [117.8174,37.4963],
			'潍坊': [119.0918,36.524],
			'烟台': [120.7397,37.5128],
			'玉溪': [101.9312,23.8898],
			'珠海': [113.7305,22.1155],
			'盐城': [120.2234,33.5577],
			'盘锦': [121.9482,41.0449],
			'石家庄': [114.4995,38.1006],
			'福州': [119.4543,25.9222],
			'秦皇岛': [119.2126,40.0232],
			'绍兴': [120.564,29.7565],
			'聊城': [115.9167,36.4032],
			'肇庆': [112.1265,23.5822],
			'舟山': [122.2559,30.2234],
			'苏州': [120.6519,31.3989],
			'莱芜': [117.6526,36.2714],
			'菏泽': [115.6201,35.2057],
			'营口': [122.4316,40.4297],
			'葫芦岛': [120.1575,40.578],
			'衡水': [115.8838,37.7161],
			'衢州': [118.6853,28.8666],
			'西宁': [101.4038,36.8207],
			'西安': [109.1162,34.2004],
			'贵阳': [106.6992,26.7682],
			'连云港': [119.1248,34.552],
			'邢台': [114.8071,37.2821],
			'邯郸': [114.4775,36.535],
			'郑州': [113.4668,34.6234],
			'鄂尔多斯': [108.9734,39.2487],
			'重庆': [107.7539,30.1904],
			'金华': [120.0037,29.1028],
			'铜川': [109.0393,35.1947],
			'银川': [106.3586,38.1775],
			'镇江': [119.4763,31.9702],
			'长春': [125.8154,44.2584],
			'长沙': [113.0823,28.2568],
			'长治': [112.8625,36.4746],
			'阳泉': [113.4778,38.0951],
			'青岛': [120.4651,36.3373],
			'韶关': [113.7964,24.7028]
		};

		option = {
		    tooltip : {
		        trigger: 'item'
		    },
			dataRange: {
			    x: 'left',
			    y: 'bottom',
			    splitList: [
			        {start: 1500, color: '#27f327'},
			        {start: 900, end: 1500, color: '#52f452'},
			        {start: 310, end: 1000, color: '#71f871'},
			        {start: 200, end: 300, color: '#89f489'},
			        {start: 5, end: 200, label: '10 到 200（自定义label）', color: '#a6f6a6'},
			        {start: 5, end: 5, label: '5（自定义特殊颜色）', color: '#c7fbc7'},
			        {start: 0, end: 5, color: '#d6f9d6'}
			    ]
			},
		    series : [
		        {
		            name: '订单数',
		            type: 'map',
		            mapType: 'china',
		            roam: false,
		            itemStyle:{
		                normal:{
		                    label:{
		                        show:true,
		                        textStyle: {
		                           color: "rgb(249, 249, 249)"
		                        }
		                    }
		                },
		                emphasis:{label:{show:true}}
		            },
		            data:[
		                {name: '北京',value: Math.round(Math.random()*2000)},
		                {name: '天津',value: Math.round(Math.random()*2000)},
		                {name: '上海',value: Math.round(Math.random()*2000)},
		                {name: '重庆',value: Math.round(Math.random()*2000)},
		                {name: '河北',value: 0},
		                {name: '河南',value: Math.round(Math.random()*2000)},
		                {name: '云南',value: 5},
		                {name: '辽宁',value: 305},
		                {name: '黑龙江',value: Math.round(Math.random()*2000)},
		                {name: '湖南',value: 200},
		                {name: '安徽',value: Math.round(Math.random()*2000)},
		                {name: '山东',value: Math.round(Math.random()*2000)},
		                {name: '新疆',value: Math.round(Math.random()*2000)},
		                {name: '江苏',value: Math.round(Math.random()*2000)},
		                {name: '浙江',value: Math.round(Math.random()*2000)},
		                {name: '江西',value: Math.round(Math.random()*2000)},
		                {name: '湖北',value: Math.round(Math.random()*2000)},
		                {name: '广西',value: Math.round(Math.random()*2000)},
		                {name: '甘肃',value: Math.round(Math.random()*2000)},
		                {name: '山西',value: Math.round(Math.random()*2000)},
		                {name: '内蒙古',value: Math.round(Math.random()*2000)},
		                {name: '陕西',value: Math.round(Math.random()*2000)},
		                {name: '吉林',value: Math.round(Math.random()*2000)},
		                {name: '福建',value: Math.round(Math.random()*2000)},
		                {name: '贵州',value: Math.round(Math.random()*2000)},
		                {name: '广东',value: Math.round(Math.random()*2000)},
		                {name: '青海',value: Math.round(Math.random()*2000)},
		                {name: '西藏',value: Math.round(Math.random()*2000)},
		                {name: '四川',value: Math.round(Math.random()*2000)},
		                {name: '宁夏',value: Math.round(Math.random()*2000)},
		                {name: '海南',value: Math.round(Math.random()*2000)},
		                {name: '台湾',value: Math.round(Math.random()*2000)},
		                {name: '香港',value: Math.round(Math.random()*2000)},
		                {name: '澳门',value: Math.round(Math.random()*2000)}
		            ],
		            markLine : {
		            	smooth:true,
		                effect : {
		                    show: true,
		                    loop:true,
		                    scaleSize: 3,
		                    period: 30,
		                    color: '#fff',
		                    shadowBlur: 10
		                },
		                itemStyle : {
		                    normal: {
		                        borderWidth:1,
		                        lineStyle: {
		                            type: 'solid',
		                            shadowBlur: 20,
		                            color: 'red'
		                        }
		                    }
		                },
		                data : [
		                    [{name:'北京'},{name:'上海'}]
		                ]
		            },
		            geoCoord: geoCoordVar
		        }
		    ]
		};
		
		// 路径配置
        require.config({
			paths: {
				echarts: 'http://echarts.baidu.com/build/dist'
			},
        
    		packages:[
	  			{
	  				name:  'echarts',
	  				location:'./js/src',
	  				main:'echarts'
	  			},
	  			{
	  				name:'zrender',
	  				location:'./zrender/src',
	  				main:'zrender'
	  			}
	  		]
		});
		                    
		require(
			[
			    'echarts',
			    'echarts/chart/map'
			    // 使用柱map模块，按需加载
			],
			function (ec) {
				require('echarts/util/mapData/params').params.HK = {
     				getGeoJson: function (callback) {
     					$.getJSON('${pageContext.request.contextPath}/json/echarts/china/china.json',callback);
     				}
     			};
				myChart = ec.init(document.getElementById('myMap'));
			    myChart.setOption(option);
			}
		);
		
		//TODO alexgaoyh
		var geoCoordName = [];
		for(var i in geoCoordVar){//用javascript的for/in循环遍历对象的属性 
			geoCoordName.push(i);
		}
		
		//保存定时任务添加的起止位置的名称 形如 ： '郑州 > 北京' 中间留有空格
		var addedGeoCoordName = [];
		
		var maxMarkLineNum = 10;
		var currentMarkLineNum = 1;
		var fromGeoCoordName, fromGeoCoordLocation, toGeoCoordName, toGeoCoordLocation;
		setInterval(function() {
			fromGeoCoordName = geoCoordName[Math.floor(Math.random()*geoCoordName.length)];
			fromGeoCoordLocation = geoCoordVar[fromGeoCoordName];
			toGeoCoordName = geoCoordName[Math.floor(Math.random()*geoCoordName.length)];
			toGeoCoordLocation = geoCoordVar[toGeoCoordName];
			
			//刷新所选区域订单数
			var myChartOption=myChart.getOption();
			var _oldSeriesData = myChartOption.series[0].data;
			for(var i in _oldSeriesData){//用javascript的for/in循环遍历对象的属性 
				if(toGeoCoordName == _oldSeriesData[i].name) {
					console.log(_oldSeriesData[i].name + ":" + _oldSeriesData[i].value);
					_oldSeriesData[i].value = _oldSeriesData[i].value + 1;
					$('#myOrderLog').append(_oldSeriesData[i].name + "订单数+1，更新为：" + _oldSeriesData[i].value + '<br>');
				}
			}
			myChartOption.series[0].data = _oldSeriesData;
			myChart.setOption(myChartOption, true);
			//myChart.setOption({series:[{type:'map',name:'订单量',data:[{name: '新疆',value: 1} ]}]});
			
			
		    // 动态数据接口 addData
		    myChart.addMarkLine(
		    		0,        // 系列索引
		            {
		                smooth:true,
		                effect : {
		                    show: true,
		                    loop:true,
		                    scaleSize: 3,
		                    period: 30,
		                    color: '#fff',
		                    shadowBlur: 10
		                },
		                itemStyle : {
		                    normal: {
		                        borderWidth:1,
		                        lineStyle: {
		                            type: 'solid',
		                            shadowBlur: 20
		                        }
		                    }
		                },
		                geoCoord: geoCoordVar,
		                data :[
		                    [{name:fromGeoCoordName}, {name:toGeoCoordName}]
		                ]
		 
		    });
		    currentMarkLineNum++;
			
			if(currentMarkLineNum > maxMarkLineNum) {
				for(var i = 0 ; i < addedGeoCoordName.length ; i++) {
					myChart.delMarkLine(0, addedGeoCoordName[i]);
				}
				addedGeoCoordName.length = 0;
				currentMarkLineNum = 1;
			}
			
			$('#myLog').append(fromGeoCoordName + ":" + fromGeoCoordLocation + "---" +  toGeoCoordName + ":" + toGeoCoordLocation + '<br>');
			var _count = $('#myAllConut').val()*1 + 1;
			if(_count%10 == 1){
				$('#myLog').html('');
			}
			$('#myAllConut').val(_count);
			var _thisGeoCoordName = fromGeoCoordName + ' > ' + toGeoCoordName;
			addedGeoCoordName.push(_thisGeoCoordName);
			
		}, 1000);
		
    </script>
</body>
</html>

