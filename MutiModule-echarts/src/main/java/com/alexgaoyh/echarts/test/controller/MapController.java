package com.alexgaoyh.echarts.test.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/map")
public class MapController {

	@RequestMapping("/dataRangeAndLine")
	public ModelAndView dataRangeAndLine(HttpServletRequest request, ModelAndView model) {
		model.setViewName("/map/dataRangeAndLine");
		return model;
	}
}
