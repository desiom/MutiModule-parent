package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.view;

import java.util.List;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.DictDictionary;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.item.DictDictionaryItem;

/**
 * 视图类 ，字典表 父子关系 视图类
 * @author alexgaoyh
 *
 */
public class DictDictionaryWithItemView extends DictDictionary{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6183313525788877804L;
	
	private List<DictDictionaryItemWithOutPrefixIdView> items;

	public List<DictDictionaryItemWithOutPrefixIdView> getItems() {
		return items;
	}

	public void setItems(List<DictDictionaryItemWithOutPrefixIdView> items) {
		this.items = items;
	}
	
	
}
