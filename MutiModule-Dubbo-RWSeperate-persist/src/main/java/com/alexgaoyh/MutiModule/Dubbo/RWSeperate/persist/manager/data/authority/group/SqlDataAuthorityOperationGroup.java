package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.group;

import com.MutiModule.common.mybatis.annotation.*;
import com.MutiModule.common.mybatis.base.BaseEntity;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "sys_data_authority_group", namespace = "com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.group.SqlDataAuthorityOperationGroupMapper", remarks = "数据权限-组 ", aliasName = "sys_data_authority_group sys_data_authority_group")
public class SqlDataAuthorityOperationGroup extends BaseEntity implements Serializable {
	/**
	 * 名称,所属表字段为sys_data_authority_group.NAME
	 */
	@MyBatisColumnAnnotation(name = "NAME", value = "sys_data_authority_group_NAME", chineseNote = "名称", tableAlias = "sys_data_authority_group")
	private String name;

	/**
	 * 描述,所属表字段为sys_data_authority_group.REMARKS
	 */
	@MyBatisColumnAnnotation(name = "REMARKS", value = "sys_data_authority_group_REMARKS", chineseNote = "描述", tableAlias = "sys_data_authority_group")
	private String remarks;

	private static final long serialVersionUID = 1L;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", name=").append(name);
		sb.append(", remarks=").append(remarks);
		sb.append("]");
		return sb.toString();
	}
}