package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.rule;

import java.util.List;
import java.util.Map;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.view.SqlDataAuthorityOperationView;

public interface SqlDataAuthorityOperationRuleMapper {
	int deleteByPrimaryKey(String id);

	int selectCountByMap(Map<Object, Object> map);

	List<SqlDataAuthorityOperationRule> selectListByMap(Map<Object, Object> map);

	int insert(SqlDataAuthorityOperationRule record);

	int insertSelective(SqlDataAuthorityOperationRule record);

	SqlDataAuthorityOperationRule selectByPrimaryKey(String id);

	int updateByPrimaryKeySelective(SqlDataAuthorityOperationRule record);

	int updateByPrimaryKey(SqlDataAuthorityOperationRule record);
	
	// alexgaoyh
	
	/**
	 * 根据map 属性，查询此属性下的所有包含 配置子模型的 数据权限属性规则
	 * @param map
	 * @return
	 */
	List<SqlDataAuthorityOperationView> selectListWithOperationModelByMap(Map<Object, Object> map);
	
	/**
	 * 根据主键ID查询实体信息，包含 明细结构
	 * @param id	主键ID
	 * @return	实体信息，包含 明细结构 数据
	 */
	SqlDataAuthorityOperationView selectEntityWithOperationModelById(String id);
}