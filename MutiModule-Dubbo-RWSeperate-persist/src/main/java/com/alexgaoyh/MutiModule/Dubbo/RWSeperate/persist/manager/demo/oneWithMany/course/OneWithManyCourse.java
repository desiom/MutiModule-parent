package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.course;

import com.MutiModule.common.mybatis.base.BaseEntity;
import java.io.Serializable;

public class OneWithManyCourse extends BaseEntity implements Serializable {
    private String name;

    private static final long serialVersionUID = 1L;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}