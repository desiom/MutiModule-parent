package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanResource;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.MutiModule.common.vo.TreeNode;
import com.MutiModule.common.vo.ZTreeNodes;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanResource.SysmanResource;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanResource.SysmanResourceMapper;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.util.tree.TreeConvert;

public class SysmanResourceTest {

	private SysmanResourceMapper mapper;

	// @Before
    public void prepare() throws Exception {
    	
        ApplicationContext ctx = new ClassPathXmlApplicationContext( "mybatis-spring-config.xml" );
        
        mapper = (SysmanResourceMapper) ctx.getBean( "sysmanResourceMapper" );
        
    }
	
	// @Test
	public void selectSysmanResourceByParentIdTest() {
		List<SysmanResource> list = mapper.selectSysmanResourceByParentId("11048197823472640");
		if(list != null) {
			for (SysmanResource sysmanResource : list) {
				System.out.println(sysmanResource.getId());
			}
		}
	}
    
	//@Test
	public void selectIconResourceByUserIdAndParentId() {
		List<SysmanResource> list = mapper.selectIconResourceByUserIdAndParentId("682814769132081152", "11002908377359360");
		System.out.println(list.size());
	}
	
	//@Test
	public void selectTreeNodeBySysmanResourceIdTest() {
		List<TreeNode> list = mapper.selectTreeNodeBySysmanResourceId("1");
		System.out.println(list.get(0).getChildren().get(0).getChildren().get(0).getHref());
	}
	
	//@Test
	public void selectAllResourceZTreeNodeTest() {
		List<ZTreeNodes> list = mapper.selectAllResourceZTreeNode();
		for(ZTreeNodes zNodes : list) {
			System.out.println(zNodes.getId() + ":" + zNodes.getpId() + ":" + zNodes.getName());
		}
	}
	
	
	//@Test
	public void selectResourceListByUserIdTest() {
		List<SysmanResource> nodes = mapper.selectResourceListByUserId("11297154310284288");
		
		List<SysmanResource> root = TreeConvert.getRootNodesList(nodes);
		
		List<TreeNode> treeNodeList = TreeConvert.convertSysmanResourceListToTreeNodeList(root, nodes);
		
		System.out.println(treeNodeList);
		
	}
	
}
