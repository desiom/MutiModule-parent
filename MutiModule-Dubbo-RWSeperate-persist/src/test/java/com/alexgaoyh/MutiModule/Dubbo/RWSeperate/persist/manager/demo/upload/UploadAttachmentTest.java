package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.upload;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.MutiModule.common.twitter.IDGenerator.instance.IdWorkerInstance;
import com.MutiModule.common.vo.enums.DeleteFlagEnum;
import com.MutiModule.common.vo.mybatis.pagination.Page;

public class UploadAttachmentTest {

	private DemoAttachmentMapper mapper;

	// @Before
    public void prepare() throws Exception {
    	
        ApplicationContext ctx = new ClassPathXmlApplicationContext( "mybatis-spring-config.xml" );
        
        mapper = (DemoAttachmentMapper) ctx.getBean( "demoAttachmentMapper" );
        
    }
	
	// @Test
	public void selectCourseWithStudentByExampleTest() {
		Map<Object , Object> map = new HashMap<Object, Object>();
		//map.put("id", 22L);
		Page page = new Page(0, 10);
		map.put("page", page);
		
		int count = mapper.selectCountByMap(map);
		List<DemoAttachment> list = mapper.selectListByMap(map);
		System.out.println("count = " + count);
		System.out.println("list.size() = " + list.size());
	}
	
	// @Test
	public void selectByPrimaryKey() {
		mapper.selectByPrimaryKey("1");
		mapper.selectByPrimaryKeyAndTableName("1", "demo_attachment");
	}
	
	// @Test
	public void deleteByPrimaryKey() {
		mapper.deleteByPrimaryKey("1");
		mapper.deleteByPrimaryKeyAndTableName("1", "demo_attachment");
	}
	
	// @Test
	public void insertUpdateEntity() {
		DemoAttachment attach = new DemoAttachment();
		attach.setId(IdWorkerInstance.getIdStr());
		attach.setName(IdWorkerInstance.getIdStr());
		attach.setCreateTime(IdWorkerInstance.getIdStr());
		attach.setDeleteFlag(DeleteFlagEnum.NORMAL);
		// attach.setDynamicTableName("demo_attachment"); //  可选
		mapper.insert(attach);
		
		attach.setId(IdWorkerInstance.getIdStr());
		
		mapper.insertSelective(attach);
		mapper.updateByPrimaryKey(attach);
		mapper.updateByPrimaryKeySelective(attach);
	}
	
	// @Test
	public void selectByMap() {
		Map<Object , Object> map = new HashMap<Object, Object>();
		// map.put("dynamicTableName", "demo_attachment1"); // 可选
		int count = mapper.selectCountByMap(map);
		System.out.println(count);
		List<DemoAttachment> list = mapper.selectListByMap(map);
		if(list != null && list.size() > 0) {
			for (DemoAttachment demoAttachment : list) {
				System.out.println(demoAttachment.getId());
			}
		}
	}
}
