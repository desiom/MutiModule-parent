package com.MutiModule.common.utils.btree.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.MutiModule.common.utils.btree.BTree;
import com.MutiModule.common.utils.btree.test.entity.IPRegion;

public class BTreeIPRegionTest {

	public static void main(String[] args) {
		BTree<IPRegion> tree = new BTree<IPRegion>();

		long beginTime1 = System.currentTimeMillis();
		try {
			InputStream is = BTreeIPRegionTest.class.getResourceAsStream("/ip2region/ip.merge.txt");
			BufferedReader tmp_reader = new BufferedReader(new InputStreamReader(is));
			String str;
			while ((str = tmp_reader.readLine()) != null) { // 判断最后一行不存在，为空结束循环
				String beginIP = str.substring(0, str.indexOf("|"));

				str = str.substring(str.indexOf("|") + 1, str.length());
				String endIP = str.substring(0, str.indexOf("|"));

				str = str.substring(str.indexOf("|") + 1, str.length());
				String region = str;

				IPRegion ipRegion = new IPRegion(beginIP, endIP, region);

				tree.insert(ipRegion);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(System.currentTimeMillis() - beginTime1);

		long beginTime2 = System.currentTimeMillis();
		IPRegion _search = new IPRegion("131.150.63.0", "131.150.79.255", "加拿大|0|0|0|0");
		boolean isMember = tree.isMember(_search);
		System.out.println(isMember);
		System.out.println(System.currentTimeMillis() - beginTime2);

		System.out.println(System.currentTimeMillis() - beginTime1);
	}
}
