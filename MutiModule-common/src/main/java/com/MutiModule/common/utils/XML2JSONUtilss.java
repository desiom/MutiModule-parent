package com.MutiModule.common.utils;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

public class XML2JSONUtilss {

	public static int PRETTY_PRINT_INDENT_FACTOR = 4;
    
    public static String xml2json(String inputXMLStr) {
    	String returnStr = null;
    	try {
            JSONObject xmlJSONObj = XML.toJSONObject(inputXMLStr);
            returnStr = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
            return returnStr;
        } catch (JSONException je) {
            System.out.println(je.toString());
            return returnStr;
        }
    }
}
