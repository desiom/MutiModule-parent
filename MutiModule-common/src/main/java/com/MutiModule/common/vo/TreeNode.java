package com.MutiModule.common.vo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * easyui 的树形结构对象
 * @author alexgaoyh
 *
 */
public class TreeNode implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String text;
	
	private boolean isLeaf;
	
	private String iconCls;
	
	private String state; //closed open
	
	private List<TreeNode> children;
	
	private Map<String,Object> attributes;
	
	/**
	 * 请求链接
	 */
	private String href;
	
	/**
	 * 父节点id
	 */
	private String parentId;
	
	/**
	 * 所有父节点ids集合
	 */
	private String parentAllIds;
	
	/**
	 * 资源类型
	 */
	private String resourceType;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isLeaf() {
		return isLeaf;
	}

	public void setLeaf(boolean isLeaf) {
		this.isLeaf = isLeaf;
	}

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<TreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}

	public Map<String, Object> getAttributes() {
		
		if(attributes == null ){
			attributes = new HashMap<String, Object>();
		}
		attributes.put("isLeaf", isLeaf) ;
		return attributes;
	}

	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getParentAllIds() {
		return parentAllIds;
	}

	public void setParentAllIds(String parentAllIds) {
		this.parentAllIds = parentAllIds;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
}
