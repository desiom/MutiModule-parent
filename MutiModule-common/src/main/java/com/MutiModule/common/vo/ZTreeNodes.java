package com.MutiModule.common.vo;

import java.io.Serializable;

public class ZTreeNodes implements Serializable{

	/**
	 * 主键id
	 */
	public Long id;
	
	/**
	 * 父节点id
	 */
	public Long pId;
	
	/**
	 * 名称
	 */
	public String name;
	
	/**
	 * 节点是否处于打开状态
	 */
	public boolean open;
	
	/**
	 * 节点是否处于选中状态
	 * checkbox复选框的选中与否
	 */
	public boolean checked;
	
	/**
	 * 节点是否处于可操作状态（checkbox的可否选中与否）
	 * true 为 disabled 状态    false为able状态
	 */
	public boolean chkDisabled = false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getpId() {
		return pId;
	}

	public void setpId(Long pId) {
		this.pId = pId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public boolean isChkDisabled() {
		return chkDisabled;
	}

	public void setChkDisabled(boolean chkDisabled) {
		this.chkDisabled = chkDisabled;
	}
	
	
}
