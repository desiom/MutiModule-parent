/**
 * File : WriteEventHandler.java <br/>
 * Author : alexgaoyh <br/>
 * Version : 1.1 <br/>
 * Date : 2017年7月5日 <br/>
 * Modify : <br/>
 * Package Name : com.MutiModule.common.codeing.reactor <br/>
 * Project Name : MutiModule-common <br/>
 * Description : <br/>
 * 
 */

package com.MutiModule.common.codeing.nio.reactor;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
/**
 * ClassName : WriteEventHandler <br/>
 * Function : TODO ADD FUNCTION. <br/>
 * Reason : TODO ADD REASON. <br/>
 * Date : 2017年7月5日 下午1:35:57 <br/>
 * 
 * @author : alexgaoyh <br/>
 * @version : 1.1 <br/>
 * @since : JDK 1.6 <br/>
 * @see
 */

public class WriteEventHandler implements EventHandler {
    public void handleEvent(SelectionKey handle) throws Exception {
        System.out.println("=====WriteEventHandler.java Write Event Handler =====");

        SocketChannel socketChannel =
                (SocketChannel) handle.channel();
        ByteBuffer inputBuffer = ByteBuffer.wrap("Hello Client!\n".getBytes());
        //ByteBuffer inputBuffer = (ByteBuffer) handle.attachment();
        socketChannel.write(inputBuffer);
        socketChannel.close();
    }
}