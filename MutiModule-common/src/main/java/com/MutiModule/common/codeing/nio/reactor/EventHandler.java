/**
 * File : EventHandler.java <br/>
 * Author : alexgaoyh <br/>
 * Version : 1.1 <br/>
 * Date : 2017年7月5日 <br/>
 * Modify : <br/>
 * Package Name : com.MutiModule.common.codeing.reactor <br/>
 * Project Name : MutiModule-common <br/>
 * Description : <br/>
 * 
 */

package com.MutiModule.common.codeing.nio.reactor;

import java.nio.channels.SelectionKey;

/**
 * ClassName : EventHandler <br/>
 * Function : TODO ADD FUNCTION. <br/>
 * Reason : TODO ADD REASON. <br/>
 * Date : 2017年7月5日 下午1:34:20 <br/>
 * 
 * @author : alexgaoyh <br/>
 * @version : 1.1 <br/>
 * @since : JDK 1.6 <br/>
 * @see
 */

public interface EventHandler {

    public void handleEvent(SelectionKey handle) throws Exception;
}