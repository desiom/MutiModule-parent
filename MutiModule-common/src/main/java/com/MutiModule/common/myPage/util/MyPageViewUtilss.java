package com.MutiModule.common.myPage.util;

import java.io.Serializable;
import java.util.List;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.myPage.MyQueryResult;

public class MyPageViewUtilss<T extends Serializable> {

	/**
	 *  封装 分页数据实体信息
	 * @param recordPerPage	每页多少条数据
	 * @param currentPage	当前页数
	 * @param totalCount	总共多少条数据
	 * @param list	当前页数的数据
	 * @return
	 */
	public static <T extends Serializable> MyPageView<T> generaterMyPageView(int recordPerPage, int currentPage, int totalCount, List<T> list) {
		MyPageView<T> pageView = new MyPageView<T>(recordPerPage, currentPage);
		MyQueryResult<T> qr = new MyQueryResult<T>(list, totalCount);
		pageView.setQueryResult(qr);
		return pageView;
	}
}
