package com.MutiModule.common.fieldValidator;

import java.util.Date;

import com.MutiModule.common.fileld.validator.Rule.Chinese;
import com.MutiModule.common.fileld.validator.Rule.DateTime;
import com.MutiModule.common.fileld.validator.Rule.EarlierThan;
import com.MutiModule.common.fileld.validator.Rule.English;
import com.MutiModule.common.fileld.validator.Rule.Equals;
import com.MutiModule.common.fileld.validator.Rule.GreaterThan;
import com.MutiModule.common.fileld.validator.Rule.LaterThan;
import com.MutiModule.common.fileld.validator.Rule.Length;
import com.MutiModule.common.fileld.validator.Rule.LessThan;
import com.MutiModule.common.fileld.validator.Rule.NonEquals;
import com.MutiModule.common.fileld.validator.Rule.NonNull;
import com.MutiModule.common.fileld.validator.Rule.Num;
import com.MutiModule.common.fileld.validator.Rule.NumberRange;
import com.MutiModule.common.fileld.validator.Rule.NumberType;
import com.MutiModule.common.fileld.validator.Rule.Regex;

public class SimpleBean {
	@English(message = "必须为英文字母", serviceLine = "simple, second")
	private String english;
	@Chinese(message = "必须为中文汉字", serviceLine = "simple")
	private String chinese;
	@NonNull
	private String none;
	@Length(max = 10,message = "长度不能大于50个字", serviceLine = "simple")
	private String string;

	@Num(value=NumberType.NUMBER, serviceLine = "simple")
	@Regex( message="输入格式错误",serviceLine="test_regex",name="销售价",value = "^(([0-9]|([1-9][0-9]{1,11}))|(([0-9]\\.\\d{1,10}|[1-9][0-9]{1,11}\\.\\d{1,10})))$")
	private String number;

	@Equals("repassword")
	private String password;
	private String repassword;

	@Equals("repassword")
	private int high;
	@Equals("high")
	private int samehigh;

	@GreaterThan("number2")
	@NonEquals("number2")
	private int number1;

	@LessThan("number1")
	private int number2;

	@Realtime.Author
	private String realtime;

	@Num(NumberType.SHORT)
	@NumberRange(max = 150, serviceLine = "simple", message = "数字超出了范围")
	@Length(min = 1, max = 2, serviceLine = "simple", message = "数字长度超出了范围")
	private String age;

	@Regex(value = "^[a-zA-Z]*$", name = "就是个正则")
	private String regex;

	@DateTime(value="yyyy-MM-dd" ,serviceLine = "simple", message = "时间类型错误")
	private String datetime;

	@EarlierThan("end")
	private Date start;

	@LaterThan("start")
	private Date end;

	@English
	@Chinese
	@Length(min = 10, max = 20)
	@NonNull
	private String data;

	public String getRealtime() {
		return realtime;
	}

	public void setRealtime(String realtime) {
		this.realtime = realtime;
	}

	public void setNone(String none) {
		this.none = none;
	}

	public String getNone() {
		return none;
	}

	public String getEnglish() {
		return english;
	}

	public void setEnglish(String english) {
		this.english = english;
	}

	public String getChinese() {
		return chinese;
	}

	public void setChinese(String chinese) {
		this.chinese = chinese;
	}

	public String getRegex() {
		return regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getString() {
		return string;
	}

	public void setString(String string) {
		this.string = string;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRepassword() {
		return repassword;
	}

	public void setRepassword(String repassword) {
		this.repassword = repassword;
	}

	public int getHigh() {
		return high;
	}

	public void setHigh(int high) {
		this.high = high;
	}

	public int getSamehigh() {
		return samehigh;
	}

	public void setSamehigh(int samehigh) {
		this.samehigh = samehigh;
	}

	public int getNumber1() {
		return number1;
	}

	public void setNumber1(int number1) {
		this.number1 = number1;
	}

	public int getNumber2() {
		return number2;
	}

	public void setNumber2(int number2) {
		this.number2 = number2;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

}
