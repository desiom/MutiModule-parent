package com.MutiModule.common.utils.poi.ss.formula.functions;

import org.junit.Test;

public class FinanceFunctionUtilssTest {

	/**
	 * =PMT(0.00515,360,-770000,0,0)
	 */
	// @Test
	public void PMT() {
		double pmt = FinanceFunctionUtilss.PMT(0.00515, 360d, -770000d, 0d, false);
		System.out.println("PMT = " + pmt);
	}
	
	/**
	 * =FV(0.00515, -770000, 4706.02, 0, FALSE)
	 */
	// @Test
	public void FV() {
		double FV = FinanceFunctionUtilss.FV(0.00515, -770000d, 4706.02d, 0d, false);
		System.out.println("FV = " + FV);
	}
	
	/**
	 * =NPER(0.00515, 4706.02, -770000, 0, FALSE)
	 */
	// @Test
	public void NPER() {
		double NPER = FinanceFunctionUtilss.NPER(0.00515, 4706.02d, -770000d, 0d, false);
		System.out.println("NPER = " + NPER);
	}
	
	/**
	 * =PV(0.00515, 360, -4706.02, 0, FALSE)
	 */
	// @Test
	public void PV() {
		double PV = FinanceFunctionUtilss.PV(0.00515, 360d, -4706.02d, 0d, false);
		System.out.println("PV = " + PV);
	}
	
	/**
	 * =DB(100,0,10,1,12)
	 */
	// @Test
	public void DB() {
		double DB = FinanceFunctionUtilss.DB(100,0,10,1,12);
		System.out.println("DB = " + DB);
	}
	
	/**
	 * =DDB(2400,0,5,1,2)
	 */
	// @Test
	public void DDB() {
		double DDB = FinanceFunctionUtilss.DDB(2400, 0, 5, 1, 2);
		System.out.println("DDB = " + DDB);
	}
	
	/**
	 * =RATE(4,10000,-30000,0,0,0.1)
	 */
	// @Test
	public void RATE() {
		double RATE = FinanceFunctionUtilss.RATE(4, 10000, -30000, 0, 0, 0.1);
		System.out.println("RATE = " + RATE);
	}
	
	/**
	 * =EFFECT(0.0525,4)
	 */
	// @Test
	public void EFFECT() {
		double EFFECT = FinanceFunctionUtilss.EFFECT(0.0525,4);
		System.out.println("EFFECT = " + EFFECT);
	}
	
}

