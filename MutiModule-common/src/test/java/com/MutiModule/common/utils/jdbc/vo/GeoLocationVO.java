package com.MutiModule.common.utils.jdbc.vo;

public class GeoLocationVO {
	
	public GeoLocationVO(String name, String lng, String lat) {
		this.name = name;
		this.lng = lng;
		this.lat = lat;
	}
	
	private String name;

	private String lng;
	
	private String lat;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}
	
	
}
