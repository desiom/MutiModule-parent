/**
 * File : PDFUtilss.java <br/>
 * Author : lenovo <br/>
 * Version : 1.1 <br/>
 * Date : 2016年12月5日 <br/>
 * Modify : <br/>
 * Package Name : com.MutiModule.common.pdf <br/>
 * Project Name : MutiModule-common <br/>
 * Description : <br/>
 * 
 */

package com.MutiModule.common.pdf;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

/**
 * ClassName : PDFUtilss <br/>
 * Function : pdf 操作实现类. <br/>
 * Reason : pdf 操作实现类. <br/>
 * Date : 2016年12月5日 下午7:08:31 <br/>
 * 
 * 	使用 Adobe Acrobat创建PDF表单
 * 	生成 表单之后，即可通过如下的方法进行操作替换，生成pdf 文档
 * 
 * 	使用 excel 生成一个 表格样本，之后转换为 pdf 文件的格式，使用 Adobe Acrobat 打开这个文件，
 * 	点击 准备表单 对表单域进行编辑，编辑完表单文本框之后，就能找到对应的匹配元素，之后使用如下的util方法即可完成对pdf文件的生成操作。
 * 
 * @author : alexgaoyh <br/>
 * @version : 1.1 <br/>
 * @since : JDK 1.6 <br/>
 * @see
 */

public class PDFUtilss {

	/**
	 * 将 pdf 表单进行转换输出
	 * 
	 * @param inputMap
	 *            需要替换的表单map 集合
	 * @param inputFilePath
	 *            pdf 表单路径
	 * @param outputFilePath
	 *            pdf 输出路径
	 * @throws FileNotFoundException
	 * @throws DocumentException
	 */
	public static void pdfReplace(Map<String, String> inputMap, String inputFilePath, String outputFilePath) throws FileNotFoundException, DocumentException {
		try {
			// 2 读入pdf表单
			PdfReader reader = new PdfReader(inputFilePath);
			// 3 根据表单生成一个新的pdf
			PdfStamper ps = new PdfStamper(reader, new FileOutputStream(outputFilePath));
			// 4 获取pdf表单
			AcroFields s = ps.getAcroFields();
			// 5给表单添加中文字体 这里采用系统字体。不设置的话，中文可能无法显示
			BaseFont bf = BaseFont.createFont("C:/WINDOWS/Fonts/simsunb.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			s.addSubstitutionFont(bf);
			// 6遍历pdf表单表格，同时给表格赋值
			Map fieldMap = s.getFields();
			Set set = fieldMap.entrySet();
			Iterator iterator = set.iterator();
			while (iterator.hasNext()) {
				Map.Entry entry = (Map.Entry) iterator.next();
				String key = (String) entry.getKey();
				System.out.println(key);
				if (inputMap.get(key) != null) {
					s.setField(key, "" + inputMap.get(key));
				}
			}
			ps.setFormFlattening(true); // 这句不能少
			ps.close();
			reader.close();
		}catch (IOException e) {
			e.printStackTrace();
		}catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	//@Test
	public void pdfCreate() {
		try {
			Map<String, String> paraMap = new HashMap<String, String>();
			paraMap.put("${alexgaoyh}", "alexgaoyh");
			pdfReplace(paraMap, "e://start.pdf", "e://end.pdf");
		}catch (FileNotFoundException e) {
			e.printStackTrace();  
		}catch (DocumentException e) {
			e.printStackTrace();  
			
		}
	}
}
