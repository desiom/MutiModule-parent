updated:

#20150225
	增加后台用户登陆页面；
	
#20150526
	后台登陆页面增加登陆校验功能，下一步对/admin下的所有请求，增加listener监听，避免未登录进行访问；
	
#20150527
	后台登陆增加filter(登陆与否状态校验)，后台管理页面顶部功能,左边sysmanResource树结构实现；
	对LoginFilter在登陆状态下重置过期时间，避免登陆后即便操作状态下，缓存也会过期；
	后台管理页面用户是否有操作当前资源的权限判定；
	
#20150528
	后台登陆页面，验证码和用户登陆信息，修改为使用session机制；
	
#20150601
	后台管理页面，增加/修改/删除 功能实现，修复后台用户登陆后，登陆信息session与redis缓存匹配的bug
	以实现RBAC相关单表CRUD,下一步实现rel关系处理
	
#20150602
	后台管理页面，rel关系处理(sysmanUser-sysmanRole-sysmanResource 部分)，
	jquery左右选择 easyui樹形控件
	
#20150701
	登陸驗證去除session部分，改為使用cookie部分
	同時去除驗證碼部分	
	
#20150713
	insertRel 关系测试，同时生成实体信息A B，并且将返回的主键id进行关联表数据插入
		Demo demo = new Demo();
		demo.setDeleteFlag(0);
		demo.setCreateTime(new Date());
		demo.setName("demo/index");
		demoService.insert(demo);
		
		Demo demo2 = new Demo();
		demo2.setDeleteFlag(0);
		demo2.setCreateTime(new Date());
		demo2.setName(demo.getId() + "");
		demoService.insert(demo2);

#20150717
	设定此模块 （MutiModule-web） 默认为后台管理模块，修改请求链接，删除无作用的 admin 部分
	
	增加 kindeditor 相关的功能，处理文件空间的部分功能，除了相关使用方法详见 MutiModule-kindeditorDemo 模块的README.md 部分
	另外多加上一个  <mvc:resources location="/kindeditor/" mapping="/kindeditor/**"/> 的配置，用来过滤掉kindeditor路径下的请求
	
	封装kindeditor相关的功能到此模块，其中要根据不同的登陆用户，传递不同的参数，用来区分不同的用户，文件空间相关，防止不同用户看到非本用户上传的文件资源
	在调用上传servlet 的时候，增加两个入参，?contextPath=admin&detailPath=anonymous
			contextPath： 项目上下文路径，用来表明是哪个项目模块的上传文件
			detailPath: 用户级别的路径，用来区分不同用户上传的资料文件，如果是未登录用户，使用 anonymous 作为入参，这样，能够区分不同用户的上传功能；
			
#20150718
	修复  首页登录时样式文件出不来：  Resource interpreted as Stylesheet but transferred with MIME type text/html
		增加默认的index.jsp 文件重定向功能
		web.xml 文件中，servlet 配置，url-pattern 的部分：   （web容器的默认servlet） 
			"/" 的设置将会将当前指定的 servlet 设为web应用的默认servlet，原来web容器的默认servlet将被覆盖
			
#20150721
	web模块LoginFilter匹配的url-pattern设定为  /* , 并且增加过滤url，防止不被需要的url被filter过滤
	
#20150722
	web模块，	http://127.0.0.1:8080/MutiModule-web/demo/page/5
	DemoController.java 类文件，增加分页相关的前台插件，添加 jquery.jqpagination.js 前台分页插件，并进行功能处理，现已完成
	
#20150727
	BaseLocationController zTree相关处理，完成异步加载zTree 功能；
		下一步进行zTree 相关CRUD功能；
		
#20150728
	增加web层 SysmanUserController 的条件筛选功能；
	
#20150803
	web层增加 GlobalIntercepter 拦截器，增加对 context_ 部分的槽在，减少jsp页面中过多的代码编写；
	
#20150804
	web层在进行单表CRUD时，使用弹出新页面的功能，而不是弹出dialog部分，详见 /demo/_blank 相关的功能，页面进行了单元测试，下一步可以进一步完善相关功能；
	
#20150921
	web层，web.xml druid 数据库连接池 增加部分监控功能
		StatViewSerlvet展示出来的监控信息比较敏感，是系统运行的内部情况，如果你需要做访问控制，可以配置allow和deny这两个参数
		
#20151110
	web 层，增加 SpringMVC form表单重复提交功能的校验功能，使用 注解方式，token唯一标示字符串保存在Cookie中（取消使用session）；
		测试过程的话，可以访问：  http://127.0.0.1:8080/MutiModule-web/demo/springmvcFormToken/add
			提交form表单之后，刷新浏览器，或者是浏览器后退按钮，都不能造成重复表单提交操作；
		