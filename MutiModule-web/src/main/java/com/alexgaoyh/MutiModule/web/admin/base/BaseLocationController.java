package com.alexgaoyh.MutiModule.web.admin.base;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.MutiModule.common.utils.JSONUtilss;
import com.MutiModule.common.utils.StringUtilss;
import com.MutiModule.common.vo.Result;
import com.MutiModule.common.vo.ZTreeNode;
import com.alexgaoyh.MutiModule.persist.base.location.BaseLocation;
import com.alexgaoyh.MutiModule.service.base.IBaseLocationService;
import com.alexgaoyh.MutiModule.web.util.ConstantsUtil;

@Controller
@RequestMapping(value="base/baseLocation")
public class BaseLocationController {

	@Resource
	private IBaseLocationService service;
	
	/**	通用方法
	 * 后台list页面
	 * 如请求地址为：   	http://localhost:8080/web/sysmanResource/list
	 * 则返回的页面应该在    /web/WEB-INF/views/sysmanResource/list.jsp
	 * @return
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(ModelAndView model) {
		RequestMapping rm = this.getClass().getAnnotation(RequestMapping.class);
		String moduleName = "";
		if (rm != null) {
			String[] values = rm.value();
			if (ArrayUtils.isNotEmpty(values)) {
				moduleName = values[0];
			}
		}
		if (moduleName.endsWith("/")) {
			moduleName = moduleName.substring(0, moduleName.length() - 1);
		}
		
		model.setViewName(moduleName + "/list");
		model.addObject("moduleName", moduleName);
		return model;
	}
	
	/**
	 * 获取 zTreeNode 格式的省市区地址格式
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value="getAnscLocationjson")
    @ResponseBody
	public void getAnscLocationjson(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		List<ZTreeNode> zTreeNodelist = new ArrayList<ZTreeNode>();
		
		String requestInputId = request.getParameter("id");
		
		System.out.println("requestInputId= " + requestInputId);
		
		if(StringUtilss.isEmpty(requestInputId)) {
			
			List<BaseLocation> _locationList = service.selectTopByParentId();
			for(BaseLocation bl : _locationList) {
				ZTreeNode zn = new ZTreeNode();
				zn.setId(bl.getId());
				zn.setIsParent(true);
				zn.setName(bl.getNameCN());
				zn.setParentId(bl.getParentId());
				zTreeNodelist.add(zn);
			}
			
		} else {
			zTreeNodelist = service.selectZTreeNodeListByParentId(Integer.parseInt(requestInputId));
		}
		
		
		JSONUtilss.renderJson(zTreeNodelist, response);
	}
	
	/** 逻辑删除方法
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="logicDelete")
    @ResponseBody
	public void logicDelete(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Result result = null;
		try {
			String id = request.getParameter("id");
			if (id != null) {
				int deleteCount = service.deleteLogicByIds(ConstantsUtil.DELETE_YES, StringUtilss.stringToIntegerArray(id.split("::")));
				result = new Result(true, "删除成功！操作" + deleteCount + "条数据");
			} else {
				result = new Result(false, "没有参数，非法操作！");
			}
		} catch (Exception e) {
			result = new Result(false, "更新失败！"+e.getMessage());
		} finally {
			JSONUtilss.renderJson(result, response);
		}
	}
	
	@RequestMapping(value="selectByPrimaryKey")
    @ResponseBody
	public void selectByPrimaryKey(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		Object enetity = null;
		
		String id = request.getParameter("id");
		if (id != null) {
			enetity = service.selectByPrimaryKey(Integer.parseInt(id));
		} 
		
		JSONUtilss.renderJson(enetity, response);
	}
	
	/**
	 * 更新实体信息
	 * @param request
	 * @param response
	 * @param entity
	 * @throws IOException
	 */
	@RequestMapping(value="updateBaseLocation")
    @ResponseBody
	public void updateBaseLocation(HttpServletRequest request, HttpServletResponse response, 
			@ModelAttribute("form") BaseLocation entity) throws IOException {
		Result result = null;
		try {
			if(entity != null && entity.getId() != null && entity.getId() != 0) {
				service.updateByPrimaryKeySelective(entity);
				result = new Result(false, "更新成功");
			} else {
				result = new Result(false, "更新失败,请选择需要更新的节点！");
			}
		} catch (Exception e) {
			result = new Result(false, "更新失败！" + e.getMessage());
		} finally {
			JSONUtilss.renderJson(result, response);
		}
	}
	
	@RequestMapping(value="getRegionHashMap")
    @ResponseBody
	public void getRegionHashMap(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		JSONUtilss.renderJson(service.getRegionHashMap(), response);
		
	}

	@RequestMapping(value="getRegionNameCN")
    @ResponseBody
	public void getRegionNameCN(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		JSONUtilss.renderJson(service.getRegionNameCN(), response);
		
	}

}
