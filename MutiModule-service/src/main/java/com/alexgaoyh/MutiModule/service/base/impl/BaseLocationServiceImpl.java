package com.alexgaoyh.MutiModule.service.base.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.MutiModule.common.vo.Pagination;
import com.MutiModule.common.vo.TreeNode;
import com.MutiModule.common.vo.ZTreeNode;
import com.alexgaoyh.MutiModule.persist.base.location.BaseLocation;
import com.alexgaoyh.MutiModule.persist.base.location.BaseLocationExample;
import com.alexgaoyh.MutiModule.persist.base.location.BaseLocationMapper;
import com.alexgaoyh.MutiModule.service.base.IBaseLocationService;

/**
 * 
 * @desc IBaseLocationService 接口实现类
 *
 * @author alexgaoyh
 */
@Service(value = "baseLocationService")
public class BaseLocationServiceImpl implements IBaseLocationService {

	@Resource(name = "baseLocationMapper")
	private BaseLocationMapper baseLocationMapper;

	//------------------get set方法 begin
	public BaseLocationMapper getBaseLocationMapper() {
		return baseLocationMapper;
	}

	public void setBaseLocationMapper(BaseLocationMapper baseLocationMapper) {
		this.baseLocationMapper = baseLocationMapper;
	}
	//------------------get set方法 end

	@Override
	public void insert(BaseLocation baseLocation) {
		baseLocationMapper.insert(baseLocation);
	}
	
	@Override
	public void insertSelective(BaseLocation baseLocation) {
		baseLocationMapper.insertSelective(baseLocation);
	}

	@Override
	public BaseLocation selectByPrimaryKey(Integer id) {
		return baseLocationMapper.selectByPrimaryKey(id);
	}
	
	@Override
	public int updateByPrimaryKeySelective(BaseLocation baseLocation) {
		return baseLocationMapper.updateByPrimaryKeySelective(baseLocation);
	}
	
	@Override
	public int countByExample(BaseLocationExample example) {
		return baseLocationMapper.countByExample(example);
	}
	
	@Override
	public List<BaseLocation> selectByExample(BaseLocationExample example) {
		return baseLocationMapper.selectByExample(example);
	}

	@Override
	public Pagination<BaseLocation> getPanigationByRowBounds(BaseLocationExample example) {
		
		int count = baseLocationMapper.countByExample(example);
		List<BaseLocation> list = baseLocationMapper.selectByExample(example);
		
		return new Pagination<BaseLocation>(count, list);
	}

	@Override
	public int updateByExampleSelective(BaseLocation record, BaseLocationExample example) {
		return baseLocationMapper.updateByExampleSelective(record, example);
	}
	
	@Override
	public int deleteByExample(BaseLocationExample example) {
		return baseLocationMapper.deleteByExample(example);
	}
	
	@Override
	public int deleteLogicByIds(Integer deleteFlag, Integer[] ids) {
		return baseLocationMapper.deleteLogicByIds(deleteFlag, ids);
	}

	@Override
	public List<TreeNode> selectTreeNodeById(Integer id) {
		return baseLocationMapper.selectTreeNodeById(id);
	}

	@Override
	public List<BaseLocation> selectListByParentId(Integer parentId) {
		return baseLocationMapper.selectListByParentId(parentId);
	}

	@Override
	public List<ZTreeNode> selectZTreeNodeListByParentId(Integer parentId) {
		return baseLocationMapper.selectZTreeNodeListByParentId(parentId);
	}

	@Override
	public List<BaseLocation> selectTopByParentId() {
		return baseLocationMapper.selectTopByParentId();
	}

	@Override
	public List<Map<String, Object>> getRegionHashMap() {
		return baseLocationMapper.getRegionHashMap();
	}

	@Override
	public List<Map<String, Object>> getRegionNameCN() {
		return baseLocationMapper.getRegionNameCN();
	}
}
