<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!-- dataTable 模板  分页栏部分，一页多少条数据-->
<div class="dataTables_length" id="DataTables_Table_0_length">
	<label>
		<select name="DataTables_Table_0_length" id="myTableLengthJspSelect" aria-controls="DataTables_Table_0" class="form-control input-sm" onchange="onclickSearch(1);">
			<c:forEach var="data" items="${myPagePerLengthEnum}" varStatus="status">
				<option value="${data.code }" <c:if test="${data.code ==  pageView.recordPerPage}">selected</c:if> >
					${ data.code }
				</option>
			</c:forEach>
		</select> 
		records per page
	</label>
</div>