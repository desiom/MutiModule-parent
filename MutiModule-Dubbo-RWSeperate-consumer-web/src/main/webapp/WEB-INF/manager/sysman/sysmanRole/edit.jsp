<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<body>
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>
					All form elements <small>With custom checbox and radion
						elements.</small>
				</h5>
			</div>
			<div class="ibox-content">
				<form method="post" class="form-horizontal" id="formHTML" action="${context_ }/${moduleName }/doUpdate">
				
					<input type="hidden" name="currentPage" id="currentPage" value="${currentPage }"></input>
					<input type="hidden" name="recordPerPage" id="recordPerPage" value="${recordPerPage }"></input>
					<input type="hidden" name="listSearchName" id="listSearchName" value="${listSearchName }"></input>
					
					
					<jsp:include page="form.jsp"></jsp:include>
					<div class="hr-line-dashed"></div>
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-2">
							<button class="btn btn-white" type="submit"
								onclick="javaScript:history.go(-1);">返回</button>
							<button class="btn btn-primary" type="submit">保存更新</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- jquery validate scripts -->
	<script src="/js/plugins/validate/jquery.validate.js"></script>
	<script src="/js/plugins/validate/additional-methods.js"></script>
	<script src="/js/${moduleName }/form-validate.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$.fn.zTree.init($("#treeDemo"), setting, zNodes);
			zTree_Menu = $.fn.zTree.getZTreeObj("treeDemo");
			var value =$('#parentId').val();  //取得父窗口要回显的值	
			var nodes = zTree_Menu.transformToArray(zTree_Menu.getNodes());   
			if (nodes.length>0) { 
				for(var i=0;i<nodes.length;i++){ 
					if(value == nodes[i].id){ 
						nodes[i].checked = true; 
						zTree_Menu.updateNode(nodes[i]); 
						var _id = nodes[i].id;
						for(var j=0;j<nodes.length;j++){ 
							if(_id == nodes[j].pId){ 
								$('#parentName').val(nodes[i].name);
							}
						}
					} 
				} 
			} 
		});
	</script>
</body>
</html>