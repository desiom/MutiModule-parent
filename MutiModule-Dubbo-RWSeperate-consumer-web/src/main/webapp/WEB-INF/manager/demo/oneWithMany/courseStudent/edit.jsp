<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<body>
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>
					All form elements <small>With custom checbox and radion
						elements.</small>
				</h5>
			</div>
			<div class="ibox-content">
				<form method="post" class="form-horizontal" id="formHTML" action="${context_ }/${moduleName }/doUpdateView">
					<input type="hidden" id="id" name="id" value="${entity.id }"></input>
					<input type="hidden" id="deleteFlag" name="deleteFlag" value="${entity.deleteFlag }"></input>
					<input type="hidden" id="createTime" name="createTime" value="${entity.createTime }"></input>
					<input type="hidden" id="currentPage" name="currentPage" value="${currentPage }"></input>
					<input type="hidden" id="recordPerPage" name="recordPerPage" value="${recordPerPage }"></input>
					<div class="form-group">
						<label class="col-sm-2 control-label">班级名称</label>
						<div class="col-sm-10">
							<input type="text" id="name" name="name" value="${entity.name }"
								class="form-control">
						</div>
					</div>				
					<div class="hr-line-dashed"></div>
					<div class="row">
			            <div class="col-lg-12">
			            	<div class="ibox float-e-margins">
			            		<div class="ibox-content">
						            <div class="">
						            <a onclick="fnClickAddRow();" href="javascript:void(0);" class="btn btn-primary ">Add a new row</a>
						            </div>
						            <table class="table table-striped table-bordered table-hover " id="editable" >
							            <thead>
							            <tr>
							                <th>学生姓名</th>
							                <th>删除</th>
							            </tr>
							            </thead>
							            <tbody>
							            	<c:forEach var="student" items="${entity.studentList}" varStatus="vs">
							            		<c:if test = "${student.id != null}">
								            		<tr>
														<td>
										            		<input type="hidden" id="studentList[${vs.index }].id" name="studentList[${vs.index }].id" value="${student.id }"  />
										            		<input type="hidden" id="studentList[${vs.index }].deleteFlag" name="studentList[${vs.index }].deleteFlag" value="${student.deleteFlag }"  />
										            		<input type="hidden" id="studentList[${vs.index }].createTime" name="studentList[${vs.index }].createTime" value="${student.createTime }"  />
															<input type="text" index="${vs.index }" id="studentList[${vs.index }].name" name="studentList[${vs.index }].name" value="${student.name }"  />
														</td>
														<td>
															<button class="btn btn-info " type="button" onclick="delThisInfo(this);"><i class="fa fa-paste"></i>删除</button>
														</td>
													</tr>
												</c:if>
							            	</c:forEach>
							            </tbody>
							            <tfoot>
							            <tr>
							                <th>学生姓名</th>
							                <th>删除</th>
							            </tr>
							            </tfoot>
            						</table>
          						</div>
				            </div>
			            </div>
			        </div>
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-2">
							<button class="btn btn-white" type="submit"
								onclick="javaScript:history.go(-1);">返回</button>
							<button class="btn btn-primary" type="submit">保存</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- Data Tables -->
	<script src="/js/plugins/jeditable/jquery.jeditable.js"></script>
    <script src="/js/plugins/dataTables/jquery.dataTables.js"></script>
	
	<!-- jquery validate scripts -->
	<script src="/js/plugins/validate/jquery.validate.js"></script>
	<script src="/js/plugins/validate/additional-methods.js"></script>
	<script src="/js/${moduleName }/form-validate.js"></script>
	<script type="text/javascript">
		var rowNumber = '${entity.studentList.size()}';
		
		$(document).ready(function() {
			/* Init DataTables */
            var oTable = $('#editable').dataTable({
                "bServerSide": false, 
                'bPaginate': false, //是否分页 
                "bProcessing": false, //datatable获取数据时候是否显示正在处理提示信息。 
                "iDisplayLength": 10, //每页显示10条记录 
                'bFilter': false, //是否使用内置的过滤功能 
                "bInfo" : false //是否显示页脚信息，DataTables插件左下角显示记录数  
            });

		});
		
		function delThisInfo(obj) {
			$(obj).parent().parent().remove();
			var table = $('#editable').DataTable();
	        table.row($(obj).parent().parent()).remove().draw();
		}
		
		function fnClickAddRow() {
            $('#editable').dataTable().fnAddData( [
                "<input type='text' id='studentList["+rowNumber+"].name' name='studentList["+rowNumber+"].name' value=''>",
                "<button class='btn btn-info ' type='button' onclick='delThisInfo(this);'><i class='fa fa-paste'></i>删除</button>"] );
            rowNumber ++ ;
        }
	</script>
</body>
</html>