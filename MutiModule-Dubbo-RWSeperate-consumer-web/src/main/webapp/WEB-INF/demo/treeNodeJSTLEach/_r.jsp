<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<c:forEach var="data" items="${treeList}" varStatus="vs">
	<c:set var="index" value="${index + 1}" scope="request" />
	<!-- 每一次循环，index+1 -->
	<tr>
		<td align="center" nowrap="nowrap">${data.id}</td>
		<td align="center" nowrap="nowrap">${data.text}</td>
		<td align="center" nowrap="nowrap">${data.leaf}</td>
		<td align="center" nowrap="nowrap"><c:out value="${level }"></c:out></td>
		<td align="center" nowrap="nowrap">
			<c:if test="${fn:length(data.children) > 0}">
				<!-- 如果有childen -->
				<c:set var="level" value="${level + 1}" scope="request" />
				<!-- 循环一次子列表，level+1 -->
				<c:set var="treeList" value="${data.children}" scope="request" />
				<!-- 注意此处，子列表覆盖treeList，在request作用域 -->
				<c:import url="_r.jsp" />
				<!-- 这就是递归了 -->
			</c:if>
		</td>
	</tr>
</c:forEach>
<c:set var="level" value="${level - 1}" scope="request" />
<!-- 退出时，level-1 -->
