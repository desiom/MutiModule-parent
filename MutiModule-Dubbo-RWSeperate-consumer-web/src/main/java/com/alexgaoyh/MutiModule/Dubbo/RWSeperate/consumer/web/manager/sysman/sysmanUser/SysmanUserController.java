package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.manager.sysman.sysmanUser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.twitter.IDGenerator.instance.IdWorkerInstance;
import com.MutiModule.common.utils.DateUtils;
import com.MutiModule.common.utils.JSONUtilss;
import com.MutiModule.common.utils.StringUtilss;
import com.MutiModule.common.vo.ZTreeNodes;
import com.MutiModule.common.vo.enums.DeleteFlagEnum;
import com.MutiModule.common.vo.mybatis.pagination.Page;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRole.read.ISysmanRoleReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanUser.read.ISysmanUserReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanUser.write.ISysmanUserWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanUserRoleRel.read.ISysmanUserRoleRelReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.base.BaseController;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRole.SysmanRole;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanUser.SysmanUser;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanUserRoleRel.SysmanUserRoleRelKey;
import com.alibaba.dubbo.config.annotation.Reference;

/**
*
* dubbo 消费者
 * SysmanUser 模块 读接口
 * @author alexgaoyh
*
*/
@Controller
@RequestMapping(value="manager/sysman/sysmanUser")
public class SysmanUserController extends BaseController<SysmanUser>{

	@Reference(group="readService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanUser.read.ISysmanUserReadService")
	private ISysmanUserReadService readService;
	
	@Reference(group="writeService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanUser.write.ISysmanUserWriteService")
	private ISysmanUserWriteService writeService;
	
	@Reference(group="readService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRole.read.ISysmanRoleReadService")
	private ISysmanRoleReadService sysmanRoleReadService;
	
	@Reference(group="readService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanUserRoleRel.read.ISysmanUserRoleRelReadService")
	private ISysmanUserRoleRelReadService userRoleRelReadService;
	
	@Override
	public ModelAndView list(ModelAndView model, HttpServletRequest request) {
		
		super.list(model, request);
		
		MyPageView<SysmanUser> pageView = decorateCondition(request);

		model.addObject("pageView", pageView);
		
		return model;
		
	}
	
	/**
	 * 封装参数部分
	 * @param request
	 * @return
	 */
	protected MyPageView<SysmanUser> decorateCondition(HttpServletRequest request) {
		
		String currentPageStr = request.getParameter("currentPage");
		if(StringUtilss.isEmpty(currentPageStr)) {
			currentPageStr = "1";
		}
		
		String recordPerPageStr = request.getParameter("recordPerPage");
		if(StringUtilss.isEmpty(recordPerPageStr)) {
			recordPerPageStr = "10";
		}

		int beginInt = Integer.parseInt(currentPageStr) >=1 ? Integer.parseInt(currentPageStr) : 1;
		
		Page page = new Page((beginInt - 1)*Integer.parseInt(recordPerPageStr), Integer.parseInt(recordPerPageStr));
		
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("page", page);
		
		//TODO 搜索条件添加
		String listSearchNameStr = request.getParameter("listSearchName");
		if(StringUtilss.isNotEmpty(listSearchNameStr)) {
			map.put("listSearchName", "%" + listSearchNameStr + "%");
		}
		
		MyPageView<SysmanUser> pageView = readService.generateMyPageViewVO(map);
		
		return pageView;
	}
	
	@Override
	public ModelAndView add(ModelAndView model, HttpServletRequest request) {
		
		super.add(model, request);
		return model;
		
	}
	
	/**
	 * 保存
	 * 
	 * @return
	 * @throws IOException
	 */
	@Override
	public ModelAndView doSave(ModelAndView model, HttpServletRequest request, SysmanUser entity, RedirectAttributesModelMap modelMap) throws Exception  {
		super.doSave(model, request, entity, modelMap);
		
		beforeDoSave(request, entity);
		writeService.insert(entity);
		afterDoSave(request, entity);

		return model;
	}

	/**
	 * 调用保存方法之前进行的方法调用
	 * @param request
	 * @param entity 对应实体信息
	 * @throws Exception
	 */
	protected void beforeDoSave(HttpServletRequest request, SysmanUser entity) throws Exception {
		entity.setId(IdWorkerInstance.getIdStr());
		entity.setDeleteFlag(DeleteFlagEnum.NORMAL);
		entity.setCreateTime(DateUtils.getGMTBasicTime());
	}
	
	/**
	 * 电泳保存方法之后进行的方法调用
	 * @param request
	 * @param entity 对应实体信息
	 * @throws Exception
	 */
	protected void afterDoSave(HttpServletRequest request, SysmanUser entity) throws Exception {
		
	}
	
	@Override
	public ModelAndView doUpdate(ModelAndView model, HttpServletRequest request, SysmanUser entity, RedirectAttributesModelMap modelMap) throws Exception {
		super.doUpdate(model, request, entity, modelMap);
		
		beforeDoUpdate(request, entity);
		writeService.updateByPrimaryKeySelective(entity);
		afterDoUpdate(request, entity);
		
		String currentPageStr = request.getParameter("currentPage");
		if(StringUtilss.isEmpty(currentPageStr)) {
			currentPageStr = "1";
		}
		
		String recordPerPageStr = request.getParameter("recordPerPage");
		if(StringUtilss.isEmpty(recordPerPageStr)) {
			recordPerPageStr = "10";
		}
		modelMap.addAttribute("currentPage", currentPageStr);
		modelMap.addAttribute("recordPerPage", recordPerPageStr);
		
		model.setViewName("redirect:list");

		return model;
	}
	
	/**
	 * 调用更新操作之前进行的操作
	 * @param request
	 * @param entity
	 * @throws Exception
	 */
	protected void beforeDoUpdate(HttpServletRequest request, SysmanUser entity) throws Exception {
		
	}
	
	/**
	 * 调用更新操作之后进行的操作
	 * @param request
	 * @param entity
	 * @throws Exception
	 */
	protected void afterDoUpdate(HttpServletRequest request, SysmanUser entity) throws Exception {
		
	}
	
	@Override
	public ModelAndView show(@PathVariable("id") String id, ModelAndView model, HttpServletRequest request, RedirectAttributesModelMap modelMap) throws Exception  {
		
		super.show(id, model, request, modelMap);
		
		SysmanUser entity = readService.selectByPrimaryKey(id);
		model.addObject("entity", entity);
		
		List<ZTreeNodes> zTreenNodesList = new ArrayList<ZTreeNodes>();
		List<SysmanRole> resourceList = sysmanRoleReadService.selectListByMap(null);
		
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("sysmanUserId", id);
		List<SysmanUserRoleRelKey> userRoleRelKeyList = userRoleRelReadService.selectListByMap(map);
		
		for(SysmanRole _role : resourceList) {
			ZTreeNodes _zTreeNodes = new ZTreeNodes();
			_zTreeNodes.setId(Long.parseLong(_role.getId()));
			if(StringUtilss.isNotEmpty(_role.getParentId())) {
				_zTreeNodes.setpId(Long.parseLong(_role.getParentId()));
			} else {
				_zTreeNodes.setpId(0L);
			}
			_zTreeNodes.setName(_role.getName());
			_zTreeNodes.setOpen(true);
			
			boolean _checked = false;
			for(SysmanUserRoleRelKey _relKey : userRoleRelKeyList) {
				if((_role.getId() + "").equals(_relKey.getSysmanRoleId())) {
					_checked = true;
				}
			}
			_zTreeNodes.setChecked(_checked);
			
			zTreenNodesList.add(_zTreeNodes);
		}
		model.addObject("zTreenNodesList", JSONUtilss.toJSon(zTreenNodesList));
		
		return model;
	}
	
	@Override
	public ModelAndView edit(@PathVariable("id") String id, ModelAndView model, HttpServletRequest request, RedirectAttributesModelMap modelMap) throws Exception  {
		
		super.edit(id, model, request, modelMap);
		
		SysmanUser entity = readService.selectByPrimaryKey(id);
		model.addObject("entity", entity);
		return model;
	}

}
