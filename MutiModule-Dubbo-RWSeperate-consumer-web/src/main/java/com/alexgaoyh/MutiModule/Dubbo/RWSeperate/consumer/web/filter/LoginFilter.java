package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.filter;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.MutiModule.common.utils.CollectionUtilss;
import com.MutiModule.common.utils.CookieUtilss;
import com.MutiModule.common.utils.DubboRWSepConWebPropUtilss;
import com.MutiModule.common.utils.RegexUtilss;
import com.MutiModule.common.utils.StringUtilss;

public class LoginFilter implements Filter {
	
	private String[] expertLoginUrl;
	private String expertLoginRegex;
	
	@Override
    public void init(FilterConfig filterConfig) throws ServletException {
		this.expertLoginRegex = filterConfig.getInitParameter("expertLoginRegex");
		this.expertLoginUrl = filterConfig.getInitParameter("expertLoginUrls").split(",");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        // 获得在下面代码中要用的request,response对象
        HttpServletRequest servletRequest = (HttpServletRequest) request;
        HttpServletResponse servletResponse = (HttpServletResponse) response;
        

        // 获得用户请求的URI
        String path = servletRequest.getRequestURI().replace(servletRequest.getContextPath(), "");
        
        // 登陆页面无需过滤 登陆方法无需过滤
		if (CollectionUtilss.contains(expertLoginUrl, path) || RegexUtilss.isMatch(expertLoginRegex, path)) {
			chain.doFilter(servletRequest, servletResponse);
			return;
		}
        
        //從web.xml裡面，取出context-param鍵值對標註的某個值
        String adminLoginCookieName = DubboRWSepConWebPropUtilss.getAdminCookie();
        
        String loginCookie = CookieUtilss.get(servletRequest, adminLoginCookieName);
        
        // 判断如果没有取到员工信息,就跳转到登陆页面
        if (StringUtilss.isEmpty(loginCookie)) {
        	// 跳转到登陆页面
            servletResponse.sendRedirect(servletRequest.getContextPath() + "/?redirect=" + URLEncoder.encode(path, "UTF-8"));
        } else {
            // 已经登陆,继续此次请求
            chain.doFilter(request, response);
        }

    }

    @Override
    public void destroy() {

    }
    
}
