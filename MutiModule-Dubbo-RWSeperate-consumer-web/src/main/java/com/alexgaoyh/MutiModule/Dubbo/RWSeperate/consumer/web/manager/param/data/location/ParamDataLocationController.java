package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.manager.param.data.location;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.MutiModule.common.utils.JSONUtilss;
import com.MutiModule.common.vo.zTree.ZTreeNodesBigDataAsync;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.param.data.location.read.IParamDataLocationReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.param.data.location.write.IParamDataLocationWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.base.BaseController;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.param.data.location.ParamDataLocation;
import com.alibaba.dubbo.config.annotation.Reference;

/**
*
* dubbo 消费者
 * ParamDataLocation 模块 读接口
 * @author alexgaoyh
*
*/
@Controller
@RequestMapping(value="manager/param/data/location")
public class ParamDataLocationController extends BaseController<ParamDataLocation>{

	@Reference(group="readService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.param.data.location.read.IParamDataLocationReadService")
	private IParamDataLocationReadService readService;
	
	@Reference(group="writeService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.param.data.location.write.IParamDataLocationWriteService")
	private IParamDataLocationWriteService writeService;
	
	@Override
	public ModelAndView list(ModelAndView model, HttpServletRequest request) {
		
		super.list(model, request);
		
		List<ZTreeNodesBigDataAsync> list = readService.selectSingleZTreeNodeListByParentId("0");
		
		System.out.println(JSONUtilss.toJSon(list));
		model.addObject("zTreeNodesList", JSONUtilss.toJSon(list));
		
		return model;
		
	}
	
	
	@RequestMapping(value = "/getZtreeNodesListById/{id}", produces = "application/json; charset=utf-8")
	@ResponseBody
	public String getZtreeNodesListById(@PathVariable String id,HttpServletRequest requestInput) {
		List<ZTreeNodesBigDataAsync> list = readService.selectSingleZTreeNodeListByParentId(id);
		return JSONUtilss.toJSon(list);
	}

}
