package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.manager.sysman.sysmanUserRoleRel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import com.MutiModule.common.utils.JSONUtilss;
import com.MutiModule.common.utils.StringUtilss;
import com.MutiModule.common.vo.ZTreeNodes;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRole.read.ISysmanRoleReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanUserRoleRel.read.ISysmanUserRoleRelReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanUserRoleRel.write.ISysmanUserRoleRelWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.base.BaseController;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanResource.SysmanResource;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRole.SysmanRole;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRoleResourceRel.SysmanRoleResourceRelKey;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanUserRoleRel.SysmanUserRoleRelKey;
import com.alibaba.dubbo.config.annotation.Reference;

/**
*
* dubbo 消费者
 * SysmanUserRoleRel 模块 读接口
 * @author alexgaoyh
*
*/
@Controller
@RequestMapping(value="manager/sysman/sysmanUserRoleRel")
public class SysmanUserRoleRelController extends BaseController<SysmanUserRoleRelKey>{

	@Reference(group="readService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanUserRoleRel.read.ISysmanUserRoleRelReadService")
	private ISysmanUserRoleRelReadService readService;
	
	@Reference(group="writeService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanUserRoleRel.write.ISysmanUserRoleRelWriteService")
	private ISysmanUserRoleRelWriteService writeService;
	
	//-------------
	@Reference(group="readService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRole.read.ISysmanRoleReadService")
	private ISysmanRoleReadService roleReadService;
	
	@RequestMapping(value = "/userRoleRelList/{id}")
	public ModelAndView userRoleRelList(@PathVariable("id") String id, ModelAndView model, HttpServletRequest request) {

		RequestMapping rm = this.getClass().getAnnotation(RequestMapping.class);
		String moduleName = "";
		if (rm != null) {
			String[] values = rm.value();
			if (ArrayUtils.isNotEmpty(values)) {
				moduleName = values[0];
			}
		}
		if (moduleName.endsWith("/")) {
			moduleName = moduleName.substring(0, moduleName.length() - 1);
		}
		
		List<ZTreeNodes> zTreenNodesList = new ArrayList<ZTreeNodes>();
		List<SysmanRole> resourceList = roleReadService.selectListByMap(null);
		
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("sysmanUserId", id);
		List<SysmanUserRoleRelKey> userRoleRelKeyList = readService.selectListByMap(map);
		
		for(SysmanRole _role : resourceList) {
			ZTreeNodes _zTreeNodes = new ZTreeNodes();
			_zTreeNodes.setId(Long.parseLong(_role.getId()));
			if(StringUtilss.isNotEmpty(_role.getParentId())) {
				_zTreeNodes.setpId(Long.parseLong(_role.getParentId()));
			} else {
				_zTreeNodes.setpId(0L);
			}
			_zTreeNodes.setName(_role.getName());
			_zTreeNodes.setOpen(true);
			
			boolean _checked = false;
			for(SysmanUserRoleRelKey _relKey : userRoleRelKeyList) {
				if((_role.getId() + "").equals(_relKey.getSysmanRoleId())) {
					_checked = true;
				}
			}
			_zTreeNodes.setChecked(_checked);
			
			zTreenNodesList.add(_zTreeNodes);
		}
		model.addObject("zTreenNodesList", JSONUtilss.toJSon(zTreenNodesList));
		model.addObject("sysmanUserId", id);
		
		String currentPageStr = request.getParameter("currentPage");
		if(StringUtilss.isEmpty(currentPageStr)) {
			currentPageStr = "1";
		}
		
		String recordPerPageStr = request.getParameter("recordPerPage");
		if(StringUtilss.isEmpty(recordPerPageStr)) {
			recordPerPageStr = "10";
		}
		model.addObject("currentPage", currentPageStr);
		model.addObject("recordPerPage", recordPerPageStr);
		
		model.addObject("moduleName", moduleName);
		model.setViewName(moduleName + "/userRoleRelList");
		
		return model;
	}

	/**
	 * 保存
	 * 
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/saveRels")
	public ModelAndView saveRels(ModelAndView model, HttpServletRequest request, RedirectAttributesModelMap modelMap) throws Exception  {
		String sysmanRoleIds = request.getParameter("sysmanRoleIds");
		String sysmanUserId = request.getParameter("sysmanUserId");
		String currentPageStr = request.getParameter("currentPage");
		String recordPerPageStr = request.getParameter("recordPerPage");
		
        writeService.refreshUserRoleRel(sysmanUserId, sysmanRoleIds);
        
        modelMap.addAttribute("currentPage", currentPageStr);
		modelMap.addAttribute("recordPerPage", recordPerPageStr);
		
		model.setViewName("redirect:/manager/sysman/sysmanUser/list");

		return model;
	}
}
