package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.intercepter;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class GlobalIntercrpter extends HandlerInterceptorAdapter {
	
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		//读取配置文件中的参数并且传递到页面上
        //request.setAttribute("staticUrl_", StaticUrlPropertiesUtilss.getStaticUrl());
        request.setAttribute("context_", request.getContextPath());
        
        Enumeration<String> parameters = request.getParameterNames();
        for(Enumeration<String> e = parameters; e.hasMoreElements(); ){
        	
        	String thisName = e.nextElement().toString();
            String thisValue = request.getParameter(thisName);
            
            request.setAttribute(thisName, thisValue);
            
        }
        
		return true;
	}
	
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		super.postHandle(request, response, handler, modelAndView);
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		super.afterCompletion(request, response, handler, ex);
	}
	
}