package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.base;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import com.MutiModule.common.myPage.enums.MyPagePerLengthEnum;
import com.MutiModule.common.vo.enums.DeleteFlagEnum;

public class BaseController<E extends Serializable> extends BaseEmptyController{
	
	@RequestMapping(value = "/list")
	public ModelAndView list(ModelAndView model, HttpServletRequest request) {

		RequestMapping rm = this.getClass().getAnnotation(RequestMapping.class);
		String moduleName = "";
		if (rm != null) {
			String[] values = rm.value();
			if (ArrayUtils.isNotEmpty(values)) {
				moduleName = values[0];
			}
		}
		if (moduleName.endsWith("/")) {
			moduleName = moduleName.substring(0, moduleName.length() - 1);
		}
		
		model.addObject("moduleName", moduleName);
		model.addObject("deleteFlagEnum", DeleteFlagEnum.values());
		
		model.addObject("myPagePerLengthEnum", MyPagePerLengthEnum.values());
		
		model.setViewName(moduleName + "/list");
		
		return model;
	}
	
	@RequestMapping(value = "/add")
	public ModelAndView add(ModelAndView model, HttpServletRequest request) {

		RequestMapping rm = this.getClass().getAnnotation(RequestMapping.class);
		String moduleName = "";
		if (rm != null) {
			String[] values = rm.value();
			if (ArrayUtils.isNotEmpty(values)) {
				moduleName = values[0];
			}
		}
		if (moduleName.endsWith("/")) {
			moduleName = moduleName.substring(0, moduleName.length() - 1);
		}
		
		model.addObject("moduleName", moduleName);
		
		model.setViewName(moduleName + "/add");
		
		return model;
	}
	
	/**
	 * 保存
	 * 
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/doSave")
	public ModelAndView doSave(ModelAndView model, HttpServletRequest request, E entity, RedirectAttributesModelMap modelMap) throws Exception  {
		model.setViewName("redirect:list");

		return model;
	}
	
	/**
	 * 更新
	 * 
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/doUpdate")
	public ModelAndView doUpdate(ModelAndView model, HttpServletRequest request, E entity, RedirectAttributesModelMap modelMap) throws Exception  {
		model.setViewName("redirect:list");

		return model;
	}
	
	/**
	 * 查看
	 * @param id
	 * @param model
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/show/{id}")
	public ModelAndView show(@PathVariable("id") String id, ModelAndView model, HttpServletRequest request, RedirectAttributesModelMap modelMap) throws Exception  {
		RequestMapping rm = this.getClass().getAnnotation(RequestMapping.class);
		String moduleName = "";
		if (rm != null) {
			String[] values = rm.value();
			if (ArrayUtils.isNotEmpty(values)) {
				moduleName = values[0];
			}
		}
		if (moduleName.endsWith("/")) {
			moduleName = moduleName.substring(0, moduleName.length() - 1);
		}
		
		model.addObject("moduleName", moduleName);
		
		model.setViewName(moduleName + "/show"); 
		
		return model;
	}
	
	/**
	 * 编辑
	 * @param id
	 * @param model
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/edit/{id}")
	public ModelAndView edit(@PathVariable("id") String id, ModelAndView model, HttpServletRequest request, RedirectAttributesModelMap modelMap) throws Exception  {
		RequestMapping rm = this.getClass().getAnnotation(RequestMapping.class);
		String moduleName = "";
		if (rm != null) {
			String[] values = rm.value();
			if (ArrayUtils.isNotEmpty(values)) {
				moduleName = values[0];
			}
		}
		if (moduleName.endsWith("/")) {
			moduleName = moduleName.substring(0, moduleName.length() - 1);
		}
		
		model.addObject("moduleName", moduleName);
		
		model.setViewName(moduleName + "/edit");
		
		return model;
	}
	
}
