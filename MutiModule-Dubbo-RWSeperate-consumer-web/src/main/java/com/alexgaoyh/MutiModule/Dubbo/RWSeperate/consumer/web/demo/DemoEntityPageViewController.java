package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.demo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.myPage.util.MyPageViewUtilss;
import com.MutiModule.common.utils.StringUtilss;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.base.BaseController;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.demo.util.DemoEntityVOUtilss;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.demo.vo.DemoEntity;

@Controller
@RequestMapping(value="manager/sitemesh")
public class DemoEntityPageViewController extends BaseController<DemoEntity>{

	@RequestMapping(value = "/index")
	public ModelAndView index(ModelAndView model, HttpServletRequest request) {
		
		model.setViewName("demo/sitemesh/index");
		return model;
	}

	@Override
	public ModelAndView list(ModelAndView model, HttpServletRequest request) {
		
		super.list(model, request);
		
		List<DemoEntity> list = DemoEntityVOUtilss.generateList();
		
		String currentPageStr = request.getParameter("currentPage");
		if(StringUtilss.isEmpty(currentPageStr)) {
			currentPageStr = "1";
		}
		
		String recordPerPageStr = request.getParameter("recordPerPage");
		if(StringUtilss.isEmpty(recordPerPageStr)) {
			recordPerPageStr = "10";
		}
		
		int currentPage = Integer.parseInt(currentPageStr);//当前页数
		int totalCount = list.size();
		int recordPerPage = Integer.parseInt(recordPerPageStr);//每页显示条数
		
		int fromIndex = (currentPage == 1) ? 0 : (currentPage -1)*recordPerPage;
		int toIndex = currentPage*recordPerPage > totalCount ? totalCount : currentPage*recordPerPage;
		
		new MyPageViewUtilss<DemoEntity>();
		MyPageView<DemoEntity> pageView = MyPageViewUtilss.generaterMyPageView(recordPerPage, currentPage, totalCount, list.subList(fromIndex, toIndex));

		model.addObject("pageView", pageView);
		
		model.setViewName("demo/sitemesh/list");
		
		return model;
		
	}
}
